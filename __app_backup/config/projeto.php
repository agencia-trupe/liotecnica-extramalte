<?php

return array(

    'name'        => 'Liotécnica',
    'title'       => 'Liotécnica :: Extracto de Malta',
    'description' => 'Extracto de Malta Liotécnica: en polvo y liquido, es un ingrediente versátil, con beneficios sensoriales, tecnológicos y de gran importancia nutricional.',
    'keywords'    => 'extracto de malta, endulzante natural, sustituto completo o parcial del jarabe de glucosa y de fructosa',
    'share_image' => asset('assets/img/layout/liotecnica.png')

);
