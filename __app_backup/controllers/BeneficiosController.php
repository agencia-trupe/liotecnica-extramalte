<?php

use \Beneficios, \Aplicacion;

class BeneficiosController extends BaseController {

    public function index()
    {
        $beneficios   = Beneficios::first();
        $aplicaciones = Aplicacion::ordenados()->get();

        return $this->view('frontend.beneficios.index', compact('beneficios', 'aplicaciones'));
    }

    public function show($id)
    {
        $beneficio = Aplicacion::findOrFail($id);

        if (Request::ajax()) {
            return View::make('frontend.beneficios.show', compact('beneficio'));
        } else {
            return Redirect::route('beneficios');
        }
    }

    public function nutricionales()
    {
        if (Request::ajax()) {
            return View::make('frontend.beneficios.nutricionales');
        } else {
            return Redirect::route('beneficios');
        }
    }

}
