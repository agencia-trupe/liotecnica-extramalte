<?php

use \Calidad;

class CalidadController extends BaseController {

    public function index()
    {
        $calidad = Calidad::first();

        return $this->view('frontend.calidad', compact('calidad'));
    }

}
