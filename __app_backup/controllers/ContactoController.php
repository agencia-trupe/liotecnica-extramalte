<?php

use \Contacto, \ContactoRecebido;

class ContactoController extends BaseController {

    public function index()
    {
        $contacto = Contacto::first();
        $contacto['paises'] = ['Afganistán', 'Akrotiri', 'Albania', 'Alemania', 'Andorra', 'Angola', 'Anguila', 'Antártida', 'Antigua y Barbuda', 'Antillas Neerlandesas', 'Arabia Saudí', 'Arctic Ocean', 'Argelia', 'Argentina', 'Armenia', 'Aruba', 'Ashmore and Cartier Islands', 'Atlantic Ocean', 'Australia', 'Austria', 'Azerbaiyán', 'Bahamas', 'Bahráin', 'Bangladesh', 'Barbados', 'Bélgica', 'Belice', 'Benín', 'Bermudas', 'Bielorrusia', 'Birmania; Myanmar', 'Bolivia', 'Bosnia y Hercegovina', 'Botsuana', 'Brasil', 'Brunéi', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Bután', 'Cabo Verde', 'Camboya', 'Camerún', 'Canadá', 'Chad', 'Chile', 'China', 'Chipre', 'Clipperton Island', 'Colombia', 'Comoras', 'Congo', 'Coral Sea Islands', 'Corea del Norte', 'Corea del Sur', 'Costa de Marfil', 'Costa Rica', 'Croacia', 'Cuba', 'Dhekelia', 'Dinamarca', 'Dominica', 'Ecuador', 'Egipto', 'El Salvador', 'El Vaticano', 'Emiratos Árabes Unidos', 'Eritrea', 'Eslovaquia', 'Eslovenia', 'España', 'Estados Unidos', 'Estonia', 'Etiopía', 'Filipinas', 'Finlandia', 'Fiyi', 'Francia', 'Gabón', 'Gambia', 'Gaza Strip', 'Georgia', 'Ghana', 'Gibraltar', 'Granada', 'Grecia', 'Groenlandia', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea Ecuatorial', 'Guinea-Bissau', 'Guyana', 'Haití', 'Honduras', 'Hong Kong', 'Hungría', 'India', 'Indian Ocean', 'Indonesia', 'Irán', 'Iraq', 'Irlanda', 'Isla Bouvet', 'Isla Christmas', 'Isla Norfolk', 'Islandia', 'Islas Caimán', 'Islas Cocos', 'Islas Cook', 'Islas Feroe', 'Islas Georgia del Sur y Sandwich del Sur', 'Islas Heard y McDonald', 'Islas Malvinas', 'Islas Marianas del Norte', 'Islas Marshall', 'Islas Pitcairn', 'Islas Salomón', 'Islas Turcas y Caicos', 'Islas Vírgenes Americanas', 'Islas Vírgenes Británicas', 'Israel', 'Italia', 'Jamaica', 'Jan Mayen', 'Japón', 'Jersey', 'Jordania', 'Kazajistán', 'Kenia', 'Kirguizistán', 'Kiribati', 'Kuwait', 'Laos', 'Lesoto', 'Letonia', 'Líbano', 'Liberia', 'Libia', 'Liechtenstein', 'Lituania', 'Luxemburgo', 'Macao', 'Macedonia', 'Madagascar', 'Malasia', 'Malaui', 'Maldivas', 'Malí', 'Malta', 'Man, Isle of', 'Marruecos', 'Mauricio', 'Mauritania', 'Mayotte', 'México', 'Micronesia', 'Moldavia', 'Mónaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Mozambique', 'Mundo', 'Namibia', 'Nauru', 'Navassa Island', 'Nepal', 'Nicaragua', 'Níger', 'Nigeria', 'Niue', 'Noruega', 'Nueva Caledonia', 'Nueva Zelanda', 'Omán', 'Pacific Ocean', 'Países Bajos', 'Pakistán', 'Palaos', 'Panamá', 'Papúa-Nueva Guinea', 'Paracel Islands', 'Paraguay', 'Perú', 'Polinesia Francesa', 'Polonia', 'Portugal', 'Puerto Rico', 'Qatar', 'Reino Unido', 'República Centroafricana', 'República Checa', 'República Democrática del Congo', 'República Dominicana', 'Ruanda', 'Rumania', 'Rusia', 'Sáhara Occidental', 'Samoa', 'Samoa Americana', 'San Cristóbal y Nieves', 'San Marino', 'San Pedro y Miquelón', 'San Vicente y las Granadinas', 'Santa Helena', 'Santa Lucía', 'Santo Tomé y Príncipe', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leona', 'Singapur', 'Siria', 'Somalia', 'Southern Ocean', 'Spratly Islands', 'Sri Lanka', 'Suazilandia', 'Sudáfrica', 'Sudán', 'Suecia', 'Suiza', 'Surinam', 'Svalbard y Jan Mayen', 'Tailandia', 'Taiwán', 'Tanzania', 'Tayikistán', 'Territorio Británico del Océano Indico', 'Territorios Australes Franceses', 'Timor Oriental', 'Togo', 'Tokelau', 'Tonga', 'Trinidad y Tobago', 'Túnez', 'Turkmenistán', 'Turquía', 'Tuvalu', 'Ucrania', 'Uganda', 'Unión Europea', 'Uruguay', 'Uzbekistán', 'Vanuatu', 'Venezuela', 'Vietnam', 'Wake Island', 'Wallis y Futuna', 'West Bank', 'Yemen', 'Yibuti', 'Zambia', 'Zimbabue'];

        return $this->view('frontend.contacto', compact('contacto'));
    }

    public function envio()
    {
        $contacto = Contacto::first();

        $prefijo       = Input::get('prefijo');
        $primer_nombre = Input::get('primer_nombre');
        $apelido       = Input::get('apelido');
        $direccion1    = Input::get('direccion1');
        $direccion2    = Input::get('direccion2');
        $ciudad        = Input::get('ciudad');
        $estado        = Input::get('estado');
        $codigo_postal = Input::get('codigo_postal');
        $pais          = Input::get('pais');
        $empresa       = Input::get('empresa');
        $telefono      = Input::get('telefono');
        $email         = Input::get('email');
        $mensaje       = Input::get('mensaje');

        $campos = array(
            'prefijo'       => $prefijo,
            'primer_nombre' => $primer_nombre,
            'apelido'       => $apelido,
            'direccion1'    => $direccion1,
            'direccion2'    => $direccion2,
            'ciudad'        => $ciudad,
            'estado'        => $estado,
            'codigo_postal' => $codigo_postal,
            'pais'          => $pais,
            'empresa'       => $empresa,
            'telefono'      => $telefono,
            'email'         => $email,
            'mensaje'       => $mensaje
        );

        $validation = Validator::make(
            $campos,
            array(
                'prefijo'       => 'required',
                'primer_nombre' => 'required',
                'apelido'       => '',
                'direccion1'    => 'required',
                'direccion2'    => '',
                'ciudad'        => 'required',
                'estado'        => 'required',
                'codigo_postal' => 'required',
                'pais'          => 'required',
                'empresa'       => 'required',
                'telefono'      => 'required',
                'email'         => 'required|email',
                'mensaje'       => 'required'
            )
        );

        if ($validation->fails())
        {
            $response = array(
                'status'  => 'fail',
                'message' => 'Rellena todos los campos correctamente.'
            );
            return Response::json($response);
        }

        if (isset($contacto->email))
        {
            Mail::send('emails.contacto', $campos, function($message) use ($campos, $contacto)
            {
                $message->to($contacto->email, Config::get('projeto.name'))
                        ->subject('[CONTACTO] '.Config::get('projeto.name'))
                        ->replyTo($campos['email'], $campos['primer_nombre']);
            });
        }

        $object = new ContactoRecebido;
        $object->prefijo       = $prefijo;
        $object->primer_nombre = $primer_nombre;
        $object->apelido       = $apelido;
        $object->direccion1    = $direccion1;
        $object->direccion2    = $direccion2;
        $object->ciudad        = $ciudad;
        $object->estado        = $estado;
        $object->codigo_postal = $codigo_postal;
        $object->pais          = $pais;
        $object->empresa       = $empresa;
        $object->telefono      = $telefono;
        $object->email         = $email;
        $object->mensaje       = $mensaje;
        $object->save();

        $response = array(
            'status'  => 'success',
            'message' => 'Mensaje enviado con suceso!'
        );
        return Response::json($response);
    }

}
