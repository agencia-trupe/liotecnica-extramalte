<?php

use \Historia;

class HistoriaController extends BaseController {

    public function index()
    {
        $historia = Historia::first();

        return $this->view('frontend.historia', compact('historia'));
    }

}
