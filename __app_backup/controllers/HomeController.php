<?php

use \Homepage;

class HomeController extends BaseController {

	public function index()
	{
        $home = Homepage::first();

		return $this->view('frontend.home', compact('home'));
	}

}
