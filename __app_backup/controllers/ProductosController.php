<?php

use \Producto, \ProductosEntrada;

class ProductosController extends BaseController {

    public function index()
    {
        $productos = Producto::with('aplicaciones')->ordenados()->get();
        $entrada   = ProductosEntrada::first();

        return $this->view('frontend.productos', compact('productos', 'entrada'));
    }

}
