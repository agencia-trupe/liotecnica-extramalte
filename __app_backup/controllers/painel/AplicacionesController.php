<?php

namespace Painel;

use \Aplicacion, \Input, \Validator, \Redirect, \Session, \CropImage;

class AplicacionesController extends BasePainelController {

    private $validation_rules = [
        'imagem'     => 'required|image',
        'nome'       => 'required',
        'descricao'  => 'required'
    ];

    private $image_config = [
        [
            'width'  => 300,
            'height' => 300,
            'path'   => 'assets/img/aplicaciones/'
        ],
        [
            'width'  => 100,
            'height' => 100,
            'path'   => 'assets/img/aplicaciones/thumb/'
        ]
    ];

    public function index()
    {
        $aplicaciones = Aplicacion::ordenados()->get();

        return $this->view('painel.aplicaciones.index', compact('aplicaciones'));
    }

    public function create()
    {
        return $this->view('painel.aplicaciones.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            Aplicacion::create($input);
            Session::flash('sucesso', 'Aplicação criada com sucesso.');

            return Redirect::route('painel.aplicaciones.index');

        } catch (\Exception $e) {

            dd($e);

            return Redirect::back()
                ->withErrors(['Erro ao criar aplicação.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $aplicacion = Aplicacion::findOrFail($id);

        return $this->view('painel.aplicaciones.edit', compact('aplicacion'));
    }

    public function update($id)
    {
        $aplicacion = Aplicacion::findOrFail($id);
        $input      = Input::all();

        $this->validation_rules['imagem'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            } else {
                unset($input['imagem']);
            }

            $aplicacion->update($input);
            Session::flash('sucesso', 'Aplicação alterada com sucesso.');

            return Redirect::route('painel.aplicaciones.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar aplicação.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Aplicacion::destroy($id);
            Session::flash('sucesso', 'Aplicação removida com sucesso.');

            return Redirect::route('painel.aplicaciones.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover aplicação.']);

        }
    }

}