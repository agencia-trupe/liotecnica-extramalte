<?php

namespace Painel;

use \Beneficios, \Input, \Validator, \Redirect, \Session;

class BeneficiosController extends BasePainelController {

    private $validation_rules = [
        'sensoriales'   => 'required',
        'tecnologicos'  => 'required',
        'nutricionales' => 'required'
    ];

    public function index()
    {
        $beneficios = Beneficios::first();

        return $this->view('painel.beneficios.index', compact('beneficios'));
    }

    public function update($id)
    {
        $beneficios = Beneficios::findOrFail($id);
        $input      = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $beneficios->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.beneficios.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}