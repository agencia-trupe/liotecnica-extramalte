<?php

namespace Painel;

use \Calidad, \Input, \Validator, \Redirect, \Session, \CropImage;

class CalidadController extends BasePainelController {

    private $validation_rules = [
        'texto1'  => 'required',
        'texto2'  => 'required',
        'imagem'  => 'image',
        'selos'   => 'image'
    ];

    public function index()
    {
        $calidad = Calidad::first();

        return $this->view('painel.calidad.index', compact('calidad'));
    }

    public function update($id)
    {
        $calidad  = Calidad::findOrFail($id);
        $input    = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', [
                    'width'  => 504,
                    'height' => null,
                    'path'   => 'assets/img/calidad/',
                    'upsize' => true
                ]);
            } else {
                unset($input['imagem']);
            }

            if (Input::hasFile('selos')) {
                $input['selos'] = CropImage::make('selos', [
                    'width'  => 504,
                    'height' => null,
                    'path'   => 'assets/img/calidad/',
                    'upsize' => true
                ]);
            } else {
                unset($input['selos']);
            }

            $calidad->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.calidad.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}