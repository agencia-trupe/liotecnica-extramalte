<?php

namespace Painel;

use \Historia, \Input, \Validator, \Redirect, \Session, \CropImage;

class HistoriaController extends BasePainelController {

    private $validation_rules = [
        'texto'  => 'required',
        'imagem' => 'image'
    ];

    public function index()
    {
        $historia = Historia::first();

        return $this->view('painel.historia.index', compact('historia'));
    }

    public function update($id)
    {
        $historia = Historia::findOrFail($id);
        $input    = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', [
                    'width'  => 557,
                    'height' => null,
                    'path'   => 'assets/img/historia/',
                    'upsize' => true
                ]);
            } else {
                unset($input['imagem']);
            }

            $historia->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.historia.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}