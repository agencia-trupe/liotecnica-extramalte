<?php

namespace Painel;

use \Homepage, \Input, \Validator, \Redirect, \Session;

class HomepageController extends BasePainelController {

    private $validation_rules = [
        'titulo'   => 'required',
        'chamada1' => 'required',
        'chamada2' => 'required'
    ];

    public function index()
    {
        $homepage = Homepage::first();

        return $this->view('painel.homepage.index', compact('homepage'));
    }

    public function update($id)
    {
        $homepage = Homepage::findOrFail($id);
        $input    = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $homepage->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.homepage.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}