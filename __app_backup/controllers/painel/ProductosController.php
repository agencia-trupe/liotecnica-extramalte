<?php

namespace Painel;

use \Producto, \Aplicacion, \Input, \Validator, \Redirect, \Session, \CropImage;

class ProductosController extends BasePainelController {

    private $validation_rules = [
        'tipo'         => 'required',
        'titulo'       => 'required',
        'imagem'       => 'required|image',
        'ingredientes' => 'required',
        'envase'       => 'required',
        'peso'         => 'required',
        'vida_util'    => 'required',
        'humedad'      => 'required',
        'kosher'       => 'required',
        'actividad'    => 'required',
    ];

    private $image_config = [
        [
            'width'  => 250,
            'height' => 160,
            'path'   => 'assets/img/productos/'
        ]
    ];

    public function index()
    {
        $productos = Producto::ordenados()->get();

        return $this->view('painel.productos.index', compact('productos'));
    }

    public function create()
    {
        $aplicaciones_list = Aplicacion::ordenados()->get();
        $aplicaciones = [];

        return $this->view('painel.productos.create', compact('aplicaciones_list', 'aplicaciones'));
    }

    public function store()
    {
        $input = Input::except('aplicaciones');

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            $aplicaciones = Input::has('aplicaciones') ? Input::get('aplicaciones') : [];

            $producto = Producto::create($input);
            $producto->aplicaciones()->sync($aplicaciones);

            Session::flash('sucesso', 'Produto criado com sucesso.');

            return Redirect::route('painel.productos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar produto.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $producto = Producto::with('aplicaciones')->findOrFail($id);
        $aplicaciones_list = Aplicacion::ordenados()->get();
        $aplicaciones = $producto->aplicaciones->lists('id');

        return $this->view('painel.productos.edit', compact('producto', 'aplicaciones_list', 'aplicaciones'));
    }

    public function update($id)
    {
        $producto = Producto::findOrFail($id);
        $input    = Input::except('aplicaciones');

        $this->validation_rules['imagem'] = 'image';
        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            } else {
                unset($input['imagem']);
            }

            $aplicaciones = Input::has('aplicaciones') ? Input::get('aplicaciones') : [];

            $producto->update($input);
            $producto->aplicaciones()->sync($aplicaciones);

            Session::flash('sucesso', 'Produto alterado com sucesso.');

            return Redirect::route('painel.productos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar produto.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Producto::destroy($id);
            Session::flash('sucesso', 'Produto removido com sucesso.');

            return Redirect::route('painel.productos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover produto.']);

        }
    }

}