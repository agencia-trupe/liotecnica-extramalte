<?php

namespace Painel;

use \ProductosEntrada, \Input, \Validator, \Redirect, \Session;

class ProductosEntradaController extends BasePainelController {

    private $validation_rules = [
        'texto'                  => 'required',
        'polvo_desc'             => 'required',
        'polvo_calorias'         => 'required',
        'polvo_carbohidratos'    => 'required',
        'polvo_fibras'           => 'required',
        'polvo_proteina'         => 'required',
        'polvo_grasas_totales'   => 'required',
        'polvo_grasas_trans'     => 'required',
        'liquido_desc'           => 'required',
        'liquido_calorias'       => 'required',
        'liquido_carbohidratos'  => 'required',
        'liquido_fibras'         => 'required',
        'liquido_proteina'       => 'required',
        'liquido_grasas_totales' => 'required',
        'liquido_grasas_trans'   => 'required'
    ];

    public function index()
    {
        $entrada = ProductosEntrada::first();

        return $this->view('painel.productosentrada.index', compact('entrada'));
    }

    public function update($id)
    {
        $entrada = ProductosEntrada::findOrFail($id);
        $input   = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $entrada->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.productos.entrada.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}