<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosEntradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productos_entrada', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('texto');

			$table->text('polvo_desc');
			$table->string('polvo_calorias');
			$table->string('polvo_carbohidratos');
			$table->string('polvo_fibras');
			$table->string('polvo_proteina');
			$table->string('polvo_grasas_totales');
			$table->string('polvo_grasas_trans');

			$table->text('liquido_desc');
			$table->string('liquido_calorias');
			$table->string('liquido_carbohidratos');
			$table->string('liquido_fibras');
			$table->string('liquido_proteina');
			$table->string('liquido_grasas_totales');
			$table->string('liquido_grasas_trans');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productos_entrada');
	}

}
