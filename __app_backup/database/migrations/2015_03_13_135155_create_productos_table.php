<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('tipo');
			$table->string('titulo');
			$table->string('imagem');
			$table->string('ingredientes');
			$table->string('envase');
			$table->string('peso');
			$table->string('vida_util');
			$table->string('humedad');
			$table->string('kosher');
			$table->string('actividad');
			$table->integer('ordem')->default(-1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productos');
	}

}
