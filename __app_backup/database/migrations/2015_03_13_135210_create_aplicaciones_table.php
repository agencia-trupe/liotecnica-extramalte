<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAplicacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('aplicaciones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('imagem');
			$table->string('nome');
			$table->text('descricao');
			$table->integer('ordem')->default(-1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('aplicaciones');
	}

}
