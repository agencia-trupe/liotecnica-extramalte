<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactosRecebidosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contactos_recebidos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('prefijo');
			$table->string('primer_nombre');
			$table->string('apelido');
			$table->string('direccion1');
			$table->string('direccion2');
			$table->string('ciudad');
			$table->string('estado');
			$table->string('codigo_postal');
			$table->string('pais');
			$table->string('empresa');
			$table->string('telefono');
			$table->string('email');
			$table->text('mensaje');
			$table->string('lido')->default('false');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contactos_recebidos');
	}

}
