<?php

class CalidadSeeder extends Seeder {

    public function run()
    {
        DB::table('calidad')->delete();

        $data = array(
            array(
                'texto1' => '...',
                'texto2' => '...',
                'imagem' => 'imagem.jpg',
                'selos' => 'selos.jpg',
            )
        );

        DB::table('calidad')->insert($data);
    }

}