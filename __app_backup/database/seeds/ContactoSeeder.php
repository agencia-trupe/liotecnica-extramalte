<?php

class ContactoSeeder extends Seeder {

    public function run()
    {
        DB::table('contacto')->delete();

        $data = array(
            array(
                'endereco' => 'Av. João Paulo I, 1098 - Embu - SP - Brasil - 06817-000',
                'telefone' => '+55 11 4785-2386 / 4785-2370',
                'email' => 'export@liotecnica.com.br',
                'google_maps' => '...'
            )
        );

        DB::table('contacto')->insert($data);
    }

}