<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsuariosSeeder');
		$this->call('HomepageSeeder');
		$this->call('HistoriaSeeder');
		$this->call('ProductosEntradaSeeder');
		$this->call('BeneficiosSeeder');
		$this->call('ProcesosSeeder');
		$this->call('CalidadSeeder');
		$this->call('ContactoSeeder');
	}

}
