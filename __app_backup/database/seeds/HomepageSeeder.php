<?php

class HomepageSeeder extends Seeder {

    public function run()
    {
        DB::table('homepage')->delete();

        $data = array(
            array(
                'titulo' => 'Extracto de Malta',
                'chamada1' => 'Naturalmente, sua melhor opção em ingredientes.',
                'chamada2' => 'Mais sabor e nutrição para seus produtos',
                'frase' => 'Um ingrediente versátil com benefícios sensoriais, tecnológicos e de grande importância nutricional.',
            )
        );

        DB::table('homepage')->insert($data);
    }

}