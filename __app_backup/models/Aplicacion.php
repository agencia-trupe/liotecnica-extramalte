<?php

class Aplicacion extends Eloquent
{

    protected $table = 'aplicaciones';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
