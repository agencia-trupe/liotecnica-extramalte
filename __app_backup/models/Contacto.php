<?php

class Contacto extends Eloquent
{

    protected $table = 'contacto';

    protected $hidden = [];

    protected $guarded = ['id'];

}
