<?php

class ContactoRecebido extends Eloquent
{

    protected $table = 'contactos_recebidos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function getCreatedAtAttribute($date)
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
    }

    public function scopeNaoLidos($query)
    {
        return $query->where('lido', '!=', 'true');
    }

}
