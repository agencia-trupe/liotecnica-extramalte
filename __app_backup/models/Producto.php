<?php

class Producto extends Eloquent
{

    protected $table = 'productos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

    public function aplicaciones()
    {
        return $this->belongsToMany('Aplicacion', 'producto_aplicacion', 'producto_id', 'aplicacion_id')->orderBy('ordem', 'ASC');
    }

}
