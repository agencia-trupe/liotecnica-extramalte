<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('nuestra-historia', ['as' => 'historia', 'uses' => 'HistoriaController@index']);

Route::get('productos', ['as' => 'productos', 'uses' => 'ProductosController@index']);

Route::get('beneficios', [
      'as' => 'beneficios',
    'uses' => 'BeneficiosController@index'
]);
Route::get('beneficios/nutricionales', [
      'as' => 'beneficios.nutricionales',
    'uses' => 'BeneficiosController@nutricionales'
]);
Route::get('beneficios/{id}', [
      'as' => 'beneficios.show',
    'uses' => 'BeneficiosController@show'
]);

Route::get('procesos', ['as' => 'procesos', 'uses' => 'ProcesosController@index']);

Route::get('calidad', ['as' => 'calidad', 'uses' => 'CalidadController@index']);

Route::get('contacto', ['as' => 'contacto', 'uses' => 'ContactoController@index']);
Route::post('contacto', ['as' => 'contacto.envio', 'uses' => 'ContactoController@envio']);


// Painel

Route::get('painel', [
    'before' => 'auth',
        'as' => 'painel.home',
      'uses' => 'Painel\HomeController@index'
]);

Route::get('painel/login', ['as' => 'painel.login', 'uses' => 'Painel\HomeController@login']);
Route::post('painel/login', ['as' => 'painel.auth', 'uses' => 'Painel\HomeController@attempt']);
Route::get('painel/logout', ['as' => 'painel.logout', 'uses' => 'Painel\HomeController@logout']);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::resource('homepage', 'Painel\HomepageController');
    Route::resource('historia', 'Painel\HistoriaController');
    Route::resource('productos/entrada', 'Painel\ProductosEntradaController');
    Route::resource('productos', 'Painel\ProductosController');
    Route::resource('aplicaciones', 'Painel\AplicacionesController');
    Route::resource('beneficios', 'Painel\BeneficiosController');
    Route::resource('procesos', 'Painel\ProcesosController');
    Route::resource('calidad', 'Painel\CalidadController');
    Route::resource('contacto/recebidos', 'Painel\ContactosRecebidosController');
    Route::resource('contacto', 'Painel\ContactoController');
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::post('ajax/order', 'Painel\AjaxController@order');
});