@section('content')

    <div id="title">
        <div class="center">
            <h2>Beneficios</h2>
        </div>
    </div>

    <main id="beneficios">
        <div class="center">
            <div class="textos">
                <div class="col">
                    <h2>Benefícios Sensoriales:</h2>
                    {{ $beneficios->sensoriales }}
                </div>

                <div class="col">
                    <h2>Benefícios Tecnológicos:</h2>
                    {{ $beneficios->tecnologicos }}
                </div>

                <div class="col">
                    <h2>Benefícios Nutricionales:</h2>
                    {{ $beneficios->nutricionales }}
                    <a href="{{ route('beneficios.nutricionales') }}" class="nutricionales">Más Beneficios Nutricionales</a>
                </div>
            </div>

            <div class="aplicaciones">
                <h6>Ventajas en Aplicación</h6>
                <div class="thumbs">
                    @foreach($aplicaciones as $aplicacion)
                        <a href="{{ route('beneficios.show', $aplicacion->id) }}" class="thumb-aplicacion">
                            <div class="imagem">
                                <img src="{{ url('assets/img/aplicaciones/thumb/'.$aplicacion->imagem) }}" alt="">
                                <div class="overlay"></div>
                            </div>
                            {{ $aplicacion->nome }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </main>

@stop