<div class="lightbox-beneficio">
    <div class="imagem">
        <img src="{{ url('assets/img/aplicaciones/'.$beneficio->imagem) }}" alt="">
    </div>
    <div class="texto">
        <h2>Benefícios</h2>
        <h1>{{ $beneficio->nome }}</h1>
        {{ $beneficio->descricao }}
    </div>
</div>