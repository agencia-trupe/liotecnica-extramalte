@section('content')

    <div id="title">
        <div class="center">
            <h2>Calidad</h2>
        </div>
    </div>

    <main id="calidad">
        <div class="center">
            <div class="left">
                {{ $calidad->texto1 }}
                <div class="imagem">
                    <img src="{{ url('assets/img/calidad/'.$calidad->imagem) }}" alt="">
                </div>
            </div>

            <div class="right">
                {{ $calidad->texto2 }}
                <div class="selos">
                    <img src="{{ url('assets/img/calidad/'.$calidad->selos) }}" alt="">
                </div>
            </div>
        </div>
    </main>

@stop