@if(!str_is('home', Route::currentRouteName()))
    <div class="footer-bg">
        <div class="center">Extracto de Malta</div>
    </div>
@endif
    <footer>
        <div class="center">
            <nav>
                <p>EXTRACTO DE MALTA</p>
                <a href="{{ route('home') }}">home</a>
                <a href="{{ route('historia') }}">nuestra historia</a>
                <a href="{{ route('productos') }}">productos</a>
            </nav>
            <nav>
                <a href="{{ route('beneficios') }}">beneficios</a>
                <a href="{{ route('procesos') }}">procesos</a>
                <a href="{{ route('calidad') }}">calidad</a>
                <a href="{{ route('contacto') }}">contacto</a>
            </nav>

            <div class="contacto">
                <p>Para saber más sobre nuestros extractos de malta, contáctenos:</p>
                <p class="tel">Tel: {{ $contacto->telefone }}</p>
                <a href="mailto:{{ $contacto->email }}">{{ $contacto->email }}</a>
            </div>

            <div class="logo">
                <img src="{{ url('assets/img/layout/liotecnica-footer.png') }}" alt="">
                <p>Extracto de Malta es un producto Liotécnica.</p>
                <a href="http://www.liotecnica.com.br">www.liotecnica.com.br</a>
            </div>

            <div class="copyright">
                <p>© {{ date('Y') }} {{ Config::get('projeto.name') }} - Reservados todos los derechos.</p>
                <p><a href="http://trupe.net" target="_blank">Creación de sitios web</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>.</p>
            </div>
        </div>
    </footer>