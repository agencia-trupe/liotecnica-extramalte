@section('content')

    <div id="title">
        <div class="center">
            <h2>Contacto</h2>
        </div>
    </div>

    <main id="contacto">
        <div class="center">
            <div class="info">
                <h3>Para saber más sobre nuestros productos, contáctenos:</h3>
                <p>Ubicación oficina y fábricas:</p>
                <p>{{ $contacto->endereco }}</p>
                <p>Tel: {{ $contacto->telefone }}</p>
                <a href="mailto:{{ $contacto->email }}">{{ $contacto->email }}</a>
                <h4>Envíenos un correo electrónico para obtener más información.</h4>

                <div class="googlemaps">
                    {{ $contacto->google_maps }}
                </div>
            </div>

            <form action="" method="post" id="form-contacto">
                <label for="prefijo">Nombre completo*</label>
                <input type="text" name="prefijo" id="prefijo" placeholder="Prefijo" required>
                <input type="text" name="primer_nombre" id="primer_nombre" placeholder="Primer Nombre" required>
                <input type="text" name="apelido" id="apelido" placeholder="Apelido">

                <label for="direccion1">Dirección*</label>
                <input type="text" name="direccion1" id="direccion1" placeholder="Dirección de la calle" required>
                <input type="text" name="direccion2" id="direccion2" placeholder="Dirección de la calle - Línea 2">
                <input type="text" name="ciudad" id="ciudad" placeholder="Ciudad" required>
                <input type="text" name="estado" id="estado" placeholder="Estado/Província" required>
                <input type="text" name="codigo_postal" id="codigo_postal" placeholder="Código postal" required>
                <select name="pais" id="pais" required>
                    <option value="" disabled selected>País</option>
                    @foreach($contacto->paises as $pais)
                    <option value="{{ $pais }}">{{ $pais }}</option>
                    @endforeach
                </select>

                <label for="empresa">Empresa*</label>
                <input type="text" name="empresa" id="empresa" placeholder="Empresa" required>
                <input type="text" name="telefono" id="telefono" placeholder="Número de teléfono" required>

                <label for="email">Correo Electrónico*</label>
                <input type="email" name="email" id="email" placeholder="minombre@ejemplo.com" required>

                <label for="mensaje">Mensaje*</label>
                <textarea name="mensaje" id="mensaje" placeholder="Mensaje" required></textarea>

                <input type="submit" value="Enviar">
                <div class="resposta-wrapper">
                    <div id="form-resposta"></div>
                </div>
            </form>
        </div>
    </main>

@stop