@section('content')

    <div id="title">
        <div class="center">
            <h2>Nuestra Historia</h2>
        </div>
    </div>

    <main id="historia">
        <div class="center">
            <div class="texto">
                {{ $historia->texto }}
            </div>
            <div class="imagem">
                <img src="{{ url('assets/img/historia/'.$historia->imagem) }}" alt="">
            </div>
        </div>
    </main>

@stop