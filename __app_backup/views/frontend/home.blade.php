@section('content')

    <main id="home">
        <div class="top">
            <div class="center">
                <div class="left">
                    <img src="{{ url('assets/img/layout/home-graos.png') }}" alt="" class="graos">
                    <img src="{{ url('assets/img/layout/home-liquido.png') }}" alt="" class="liquido">
                    <img src="{{ url('assets/img/layout/home-polvo.png') }}" alt="" class="polvo">
                </div>
                <div class="right">
                    <h2>{{ $home->titulo }}</h2>
                    <h3>{{ $home->chamada1 }}</h3>
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="center">
                <div class="right">
                    <h3>{{ $home->chamada2 }}</h3>
                    <div class="aplicacoes">
                        <img src="{{ url('assets/img/layout/home-aplicacoes1.png') }}" alt="">
                        <img src="{{ url('assets/img/layout/home-aplicacoes2.png') }}" alt="">
                        <img src="{{ url('assets/img/layout/home-aplicacoes3.png') }}" alt="">
                        <img src="{{ url('assets/img/layout/home-aplicacoes4.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </main>

@stop