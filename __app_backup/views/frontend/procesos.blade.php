@section('content')

    <div id="title">
        <div class="center">
            <h2>Procesos</h2>
        </div>
    </div>

    <main id="procesos">
        <div class="center">
            <div class="texto">
                <h3>Etapas del proceso de Producción del Extracto de Malta</h3>
                {{ $procesos->texto }}
            </div>

            <div class="infografico">
                <div class="loading"></div>
                <div class="content">
                    <div class="wrapper">
                        <div class="col">
                            <img src="{{ url('assets/img/layout/procesos/a1.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/a3.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/a5.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/b2.gif') }}" alt="">
                        </div>
                        <div class="col">
                            <img src="{{ url('assets/img/layout/procesos/a2.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/a4.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/b1.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/b3.gif') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@stop