@section('content')

    <div id="title">
        <div class="center">
            <h2>Productos</h2>
        </div>
    </div>

    <main id="productos">
        <div class="entrada center">
            <div class="left">
                <h3>Extracto de Malta</h3>
                <h4>Formatos de presentación: Polvo y Líquido</h4>
                <div class="formato polvo">
                    <h5>Polvo</h5>
                    {{ $entrada->polvo_desc }}
                    <img src="{{ url('assets/img/layout/productos-polvo.png') }}" alt="">
                </div>
                <div class="formato liquido">
                    <h5>Líquido</h5>
                    {{ $entrada->liquido_desc }}
                    <img src="{{ url('assets/img/layout/productos-liquido.png') }}" alt="">
                </div>
            </div>

            <div class="right">
                <div class="texto">
                    {{ $entrada->texto }}
                </div>
                <div class="tabela">
                    <h4>Composición Nutricional Típica</h4>
                    <table>
                        <thead>
                            <th></th>
                            <th>Extracto de Malta en polvo</th>
                            <th>Extracto de Malta líquido</th>
                        </thead>
                        <tr>
                            <td>Calorias (kcal)</td>
                            <td>{{ $entrada->polvo_calorias }}</td>
                            <td>{{ $entrada->liquido_calorias }}</td>
                        </tr>
                        <tr>
                            <td>Carbohidratos (g)</td>
                            <td>{{ $entrada->polvo_carbohidratos }}</td>
                            <td>{{ $entrada->liquido_carbohidratos }}</td>
                        </tr>
                        <tr>
                            <td>Fibras (g)</td>
                            <td>{{ $entrada->polvo_fibras }}</td>
                            <td>{{ $entrada->liquido_fibras }}</td>
                        </tr>
                        <tr>
                            <td>Proteínas (g)</td>
                            <td>{{ $entrada->polvo_proteina }}</td>
                            <td>{{ $entrada->liquido_proteina }}</td>
                        </tr>
                        <tr>
                            <td>Grasas Totales (g)</td>
                            <td>{{ $entrada->polvo_grasas_totales }}</td>
                            <td>{{ $entrada->liquido_grasas_totales }}</td>
                        </tr>
                        <tr>
                            <td>Grasas Trans (g)</td>
                            <td>{{ $entrada->polvo_grasas_trans }}</td>
                            <td>{{ $entrada->liquido_grasas_trans }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="productos center">
            <div class="gutter-sizer"></div>
            <div class="grid-sizer"></div>
@if(count($productos))
@foreach($productos as $producto)
            <div class="item">
                <div class="thumb">
                    <div class="imagem">
                        <img src="{{ url('assets/img/productos/'.$producto->imagem) }}" alt="">
                    </div>
                    <h5>{{ $producto->titulo }}</h5>
                </div>
                <div class="content">
                    <div class="desc">
                        <div class="imagem">
                            <img src="{{ url('assets/img/productos/'.$producto->imagem) }}" alt="">
                        </div>

                        <div class="titulo">
                            <h5>{{ $producto->titulo }}</h5>
                            <h6>Extracto de Malta {{ ($producto->tipo == 'polvo' ? 'en polvo' : 'líquido') }}</h6>
                        </div>

                        <table>
                            <tr>
                                <td class="title" nowrap>Ingredientes</td>
                                <td>{{ $producto->ingredientes }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>Envase</td>
                                <td>{{ $producto->envase }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>Peso</td>
                                <td>{{ $producto->peso }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>Vida útil</td>
                                <td>{{ $producto->vida_util }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>Humedad (%)</td>
                                <td>{{ $producto->humedad }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>Certificado Kosher</td>
                                <td>{{ $producto->kosher }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>Actividad Diastásica</td>
                                <td>{{ $producto->actividad }}</td>
                            </tr>
                        </table>
                    </div>

                    <div class="aplicaciones">
                        <h6>Aplicaciones Típicas</h6>
                        <div class="thumbs">
                            @foreach($producto->aplicaciones as $aplicacion)
                                <a href="{{ route('beneficios.show', $aplicacion->id) }}" class="thumb-aplicacion">
                                    <div class="imagem">
                                        <img src="{{ url('assets/img/aplicaciones/thumb/'.$aplicacion->imagem) }}" alt="">
                                        <div class="overlay"></div>
                                    </div>
                                    {{ $aplicacion->nome }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
@endforeach
@endif
        </div>
    </main>

@stop