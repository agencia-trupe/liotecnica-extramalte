@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('nome', 'Nome') }}
    {{ Form::text('nome', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('descricao', 'Descrição') }}
    {{ Form::textarea('descricao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'aplicacao']) }}
</div>

<div class="well form-group">
@if($submitText == 'Alterar')
    {{ Form::label('imagem', 'Alterar imagem') }}
    <img src="{{ url('assets/img/aplicaciones/'.$aplicacion->imagem) }}" style="display:block;margin-bottom:10px;" class="img-responsive">
@else
    {{ Form::label('imagem', 'Imagem') }}
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.aplicaciones.index') }}" class="btn btn-default btn-voltar">Voltar</a>
