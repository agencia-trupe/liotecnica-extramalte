@section('content')

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Aplicações
            <a href="{{ route('painel.aplicaciones.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Aplicação</a>
        </h2>
    </legend>

    @if(count($aplicaciones))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="aplicaciones">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Nome</th>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($aplicaciones as $aplicacion)

            <tr class="tr-row" id="id_{{ $aplicacion->id }}">
                <td style="width:74px;text-align:center;">
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $aplicacion->nome }}</td>
                <td><img src="{{ url('assets/img/aplicaciones/'.$aplicacion->imagem) }}" style="max-width:100px;height:auto;" class="img-responsive"></td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.aplicaciones.destroy', $aplicacion->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.aplicaciones.edit', $aplicacion->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma aplicação cadastrada.</div>
    @endif

@stop