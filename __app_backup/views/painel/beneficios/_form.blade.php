@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('sensoriales', 'Sensoriales') }}
    {{ Form::textarea('sensoriales', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
</div>

<div class="form-group">
    {{ Form::label('tecnologicos', 'Tecnológicos') }}
    {{ Form::textarea('tecnologicos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
</div>

<div class="form-group">
    {{ Form::label('nutricionales', 'Nutricionales') }}
    {{ Form::textarea('nutricionales', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}