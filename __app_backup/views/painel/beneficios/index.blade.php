@section('content')

    <legend>
        <h2>Beneficios</h2>
    </legend>

    {{ Form::model($beneficios, [
        'route' => ['painel.beneficios.update', $beneficios->id],
        'method' => 'patch'])
    }}

        @include('painel.beneficios._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop