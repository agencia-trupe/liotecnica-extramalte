@section('content')

    <legend>
        <h2>Calidad</h2>
    </legend>

    {{ Form::model($calidad, [
        'route' => ['painel.calidad.update', $calidad->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.calidad._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop