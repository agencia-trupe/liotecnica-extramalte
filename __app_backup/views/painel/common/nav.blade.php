<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url() }}">{{ Config::get('projeto.name') }}</a>
        </div>

    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li @if(str_is('painel.homepage*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.homepage.index') }}">Home</a>
            </li>

            <li @if(str_is('painel.historia*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.historia.index') }}">Nuestra Historia</a>
            </li>

            <li @if(str_is('painel.productos*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.productos.index') }}">Productos</a>
            </li>

            <li @if(str_is('painel.aplicaciones*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.aplicaciones.index') }}">Aplicaciones</a>
            </li>

            <li @if(str_is('painel.beneficios*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.beneficios.index') }}">Beneficios</a>
            </li>

            <li @if(str_is('painel.procesos*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.procesos.index') }}">Procesos</a>
            </li>

            <li @if(str_is('painel.calidad*',Route::currentRouteName())) class="active" @endif>
                <a href="{{ route('painel.calidad.index') }}">Calidad</a>
            </li>

            <li class="dropdown @if(str_is('painel.contacto*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contacto @if($recebidos_count>=1)<span class="badge">{{ $recebidos_count }}</span>@endif <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.contacto.index') }}">Informações de Contato</a></li>
                    <li><a href="{{ route('painel.contacto.recebidos.index') }}">Contatos Recebidos @if($recebidos_count>=1)<span class="badge">{{ $recebidos_count }}</span>@endif</a></li>
                </ul>
            </li>

            <li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('painel.usuarios.index') }}">Usuários</a></li>
                    <li><a href="{{ route('painel.logout') }}">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
    </div>
</nav>