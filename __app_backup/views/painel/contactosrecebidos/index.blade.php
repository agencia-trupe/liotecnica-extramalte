@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>Contatos Recebidos</h2>
    </legend>

    @if(count($contactos_recebidos))
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Data</th>
                <th>Primer Nombre</th>
                <th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($contactos_recebidos as $contacto)

            <tr class="tr-row @if($contacto->lido != 'true') warning @endif">
                <td>{{ $contacto->created_at }}</td>
                <td>{{ $contacto->primer_nombre }}</td>
                <td>{{ $contacto->email }}</td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.contacto.recebidos.destroy', $contacto->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.contacto.recebidos.show', $contacto->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhum contato recebido.</div>
    @endif

@stop