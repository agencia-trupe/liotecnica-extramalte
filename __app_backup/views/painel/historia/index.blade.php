@section('content')

    <legend>
        <h2>Nuestra Historia</h2>
    </legend>

    {{ Form::model($historia, [
        'route' => ['painel.historia.update', $historia->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.historia._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop