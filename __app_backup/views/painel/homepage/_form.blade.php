@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('chamada1', 'Chamada 1') }}
    {{ Form::text('chamada1', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('chamada2', 'Chamada 2') }}
    {{ Form::text('chamada2', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}