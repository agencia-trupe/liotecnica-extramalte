@section('content')

    <legend>
        <h2>Home</h2>
    </legend>

    {{ Form::model($homepage, [
        'route' => ['painel.homepage.update', $homepage->id],
        'method' => 'patch'])
    }}

        @include('painel.homepage._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop