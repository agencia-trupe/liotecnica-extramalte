@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('tipo', 'Tipo') }}
    {{ Form::select('tipo', ['polvo' => 'Polvo', 'liquido' => 'Líquido'], null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="well form-group">
@if($submitText == 'Alterar')
    {{ Form::label('imagem', 'Alterar imagem') }}
    <img src="{{ url('assets/img/productos/'.$producto->imagem) }}" style="display:block;margin-bottom:10px;" class="img-responsive">
@else
    {{ Form::label('imagem', 'Imagem') }}
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

<div class="well">
    <label>Aplicações</label>
    @if(sizeof($aplicaciones_list))
        @foreach ($aplicaciones_list as $aplicacion)
        <div class="checkbox">
            <label>
                <input type="checkbox" name="aplicaciones[]" value="{{ $aplicacion->id }}" @if(in_array($aplicacion->id, $aplicaciones)) checked="checked" @endif>
                {{ $aplicacion->nome }}
            </label>
        </div>
        @endforeach
    @else
        <div class="alert alert-info" role="alert" style="margin:5px 0 0;">Nenhuma aplicação cadastrada.</div>
    @endif
</div>

<div class="form-group">
    {{ Form::label('ingredientes', 'Ingredientes') }}
    {{ Form::text('ingredientes', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('envase', 'Envase') }}
    {{ Form::text('envase', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('peso', 'Peso') }}
    {{ Form::text('peso', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('vida_util', 'Vida útil') }}
    {{ Form::text('vida_util', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('humedad', 'Humedad (%)') }}
    {{ Form::text('humedad', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('kosher', 'Certificado Kosher') }}
    {{ Form::text('kosher', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('actividad', 'Actividad diastásica') }}
    {{ Form::text('actividad', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.productos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
