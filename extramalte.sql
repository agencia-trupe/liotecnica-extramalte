-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 10.129.76.12
-- Generation Time: 31-Mar-2015 às 10:05
-- Versão do servidor: 5.6.23-log
-- PHP Version: 5.5.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `extramalte`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aplicaciones`
--

CREATE TABLE IF NOT EXISTS `aplicaciones` (
`id` int(10) unsigned NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `aplicaciones`
--

INSERT INTO `aplicaciones` (`id`, `imagem`, `nome`, `descricao`, `ordem`, `created_at`, `updated_at`) VALUES
(1, '20150320174227_bebidas-lacteas.jpg', 'Bebidas lácteas / Bebidas UHT / Bebidas achocolatadas', '<ul>\r\n	<li>\r\n	<p>Redondea y complementa el sabor del chocolate</p>\r\n	</li>\r\n	<li>\r\n	<p>Oculta notas indeseables provenientes del cacao</p>\r\n	</li>\r\n	<li>\r\n	<p>Endulza de forma suave y natural</p>\r\n	</li>\r\n	<li>\r\n	<p>Proporciona un sabor malteado dependiendo de la dosis utilizada</p>\r\n	</li>\r\n	<li>\r\n	<p>Proporciona buen cuerpo y sensaci&oacute;n de cremosidad</p>\r\n	</li>\r\n	<li>\r\n	<p>Fortalece los conceptos de nutrici&oacute;n, naturalidad y salud</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 2,0%</em></p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa para productos con sabor malteado: Empezar con 10,0%</em></p>\r\n', 0, '2015-03-17 15:24:41', '2015-03-25 23:46:25'),
(2, '20150317122454_barra-de-cereales.jpg', 'Barra de Cereales', '<ul>\r\n	<li>Sustituto completo o parcial del jarabe de glucosa y de fructosa</li>\r\n	<li>Proporciona un aporte nutricional superior</li>\r\n	<li>Endulza de forma suave y natural</li>\r\n	<li>Contribuye al concepto de producto saludable y natural</li>\r\n	<li>Act&uacute;a como aglutinante</li>\r\n	<li>Proporciona un sabor malteado dependiendo de la dosis utilizada</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa como reemplazo total de jarabe de glucosa: 20%</em></p>\r\n', 1, '2015-03-17 15:24:54', '2015-03-25 23:46:44'),
(3, '20150317122508_cereales-para-desayuno.jpg', 'Cereales para el desayuno y Granolas', '<ul>\r\n	<li>Favorece la reacci&oacute;n de <em>Maillard</em>, desarrollando una coloraci&oacute;n dorada y uniforme, con un sabor caracter&iacute;stico</li>\r\n	<li>Resalta el sabor de los cereales con sabor a chocolate</li>\r\n	<li>Contribuye al concepto de producto saludable y natural</li>\r\n	<li>Proporciona un aporte nutricional superior al de otros endulzantes</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 10,0%</em></p>\r\n', 2, '2015-03-17 15:25:08', '2015-03-25 23:50:12'),
(4, '20150317122524_productos-panaderia.jpg', 'Productos de Panadería', '<ul>\r\n	<li>Favorece la reacci&oacute;n de <em>Maillard</em>, desarrollando coloraci&oacute;n dorada y uniforme de la corteza</li>\r\n	<li>Complementa el sabor y oculta notas indeseables</li>\r\n	<li>Favorece la textura blanda de los productos a lo largo de su vida &uacute;til</li>\r\n	<li>Fuente de az&uacute;cares fermentables</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 1,5%</em></p>\r\n', 3, '2015-03-17 15:25:24', '2015-03-25 23:50:19'),
(5, '20150317122533_crackers.jpg', 'Crackers', '<ul>\r\n	<li>Aumenta su crocancia</li>\r\n	<li>Contribuye a la complementaci&oacute;n del sabor</li>\r\n	<li>Favorece la reacci&oacute;n de <em>Maillard</em>, desarrollando coloraci&oacute;n dorada y uniforme</li>\r\n	<li>Ayuda en la fermentaci&oacute;n debido a su concentraci&oacute;n de az&uacute;cares fermentables</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 0,5%</em></p>\r\n\r\n<p>&nbsp;</p>\r\n', 4, '2015-03-17 15:25:33', '2015-03-25 23:49:47'),
(6, '20150317122543_galetas-dulces.jpg', 'Galletas dulces', '<ul>\r\n	<li>Aumenta su crocancia</li>\r\n	<li>Acent&uacute;a el sabor</li>\r\n	<li>Puede ser utilizado como sustituto parcial del az&uacute;car, proporcionando un aporte nutricional superior</li>\r\n	<li>Favorece la reacci&oacute;n de <em>Maillard</em>, desarrollando coloraci&oacute;n dorada y uniforme</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 3,0%</em></p>\r\n', 5, '2015-03-17 15:25:43', '2015-03-25 23:49:57'),
(7, '20150317122552_chocolate.jpg', 'Chocolates', '<ul>\r\n	<li>Resalta el sabor del chocolate</li>\r\n	<li>Proporciona un sabor malteado</li>\r\n	<li>Oculta notas indeseables provenientes del cacao</li>\r\n	<li>Equilibra la sensaci&oacute;n de dulzor y armoniza su sabor, principalmente en chocolates blancos</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 3,0%</em></p>\r\n', 6, '2015-03-17 15:25:52', '2015-03-25 23:47:15'),
(8, '20150317122600_helados.jpg', 'Helados', '<ul>\r\n	<li>Resalta y complementa el sabor</li>\r\n	<li>Endulza de forma suave y natural</li>\r\n	<li>Oculta notas indeseables</li>\r\n	<li>Proporciona un sabor malteado dependiendo de la dosis utilizada</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 3,0%</em></p>\r\n', 7, '2015-03-17 15:26:00', '2015-03-25 23:47:20'),
(9, '20150317122612_chocolate-polvo.jpg', 'Chocolate en polvo', '<ul>\r\n	<li>Resalta y complementa el sabor del chocolate</li>\r\n	<li>Oculta notas indeseables provenientes del cacao</li>\r\n	<li>Endulza de forma suave y natural</li>\r\n	<li>Proporciona un sabor malteado dependiendo de la dosis utilizada</li>\r\n	<li>Proporciona buen cuerpo y sensaci&oacute;n de cremosidad</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 2,0%<br />\r\nDosificaci&oacute;n indicativa para productos con sabor malteado: Empezar con 10,0%</em></p>\r\n\r\n<p>&nbsp;</p>\r\n', 8, '2015-03-17 15:26:12', '2015-03-25 23:47:27'),
(10, '20150317122619_postres.jpg', 'Postres', '<ul>\r\n	<li>Complementa el sabor</li>\r\n	<li>Endulza de forma suave y natural</li>\r\n	<li>Oculta notas indeseables provenientes del cacao</li>\r\n	<li>Proporciona un sabor malteado</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Dosificaci&oacute;n indicativa: Empezar con 5,0%</em></p>\r\n', 9, '2015-03-17 15:26:20', '2015-03-25 23:47:38'),
(11, '20150317122628_cervezas.jpg', 'Cervezas', '<ul>\r\n	<li>Puede ser utilizado como sustituto completo de la malta, eliminando las etapas de molienda y maceraci&oacute;n</li>\r\n	<li>Puede ser utilizado para aumentar la concentraci&oacute;n del mosto en la fabricaci&oacute;n de cervezas con un alto contenido alcoh&oacute;lico</li>\r\n	<li>Posee un perfil ideal en t&eacute;rminos de az&uacute;cares fermentables</li>\r\n	<li>Si es utilizado con el mosto de cerveza, puede aumentar la capacidad de producci&oacute;n de la cervecer&iacute;a sin la necesidad de inversiones</li>\r\n	<li>Puede ser utilizado en la propagaci&oacute;n de levaduras</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><em>Sustituye parcial o totalmente la malta.</em></p>\r\n\r\n<p>&nbsp;</p>\r\n', 10, '2015-03-17 15:26:28', '2015-03-25 23:47:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `beneficios`
--

CREATE TABLE IF NOT EXISTS `beneficios` (
`id` int(10) unsigned NOT NULL,
  `sensoriales` text COLLATE utf8_unicode_ci NOT NULL,
  `tecnologicos` text COLLATE utf8_unicode_ci NOT NULL,
  `nutricionales` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `beneficios`
--

INSERT INTO `beneficios` (`id`, `sensoriales`, `tecnologicos`, `nutricionales`, `created_at`, `updated_at`) VALUES
(1, '<ul>\r\n	<li>\r\n	<p>Sabor t&iacute;pico de malta, aroma suave y agradable</p>\r\n	</li>\r\n	<li>\r\n	<p>Edulcoraci&oacute;n suave de origen natural, fabricado a partir de&nbsp; granos integrales</p>\r\n	</li>\r\n	<li>\r\n	<p>Resalta y complementa el sabor de los alimentos</p>\r\n	</li>\r\n	<li>\r\n	<p>Aporta crocancia y favorece el pardeamiento</p>\r\n	</li>\r\n</ul>\r\n', '<ul>\r\n	<li>\r\n	<p>Sustrato para la fermentaci&oacute;n en los productos de panader&iacute;a</p>\r\n	</li>\r\n	<li>\r\n	<p>Favorece la reacci&oacute;n de Maillard, desarrollando una coloraci&oacute;n dorada y uniforme,</p>\r\n	</li>\r\n	<li>\r\n	<p>Redondea sabores y oculta notas indeseables en productos con cacao.</p>\r\n	</li>\r\n	<li>\r\n	<p>Retiene la humedad de los productos de panader&iacute;a, aumentando vida &uacute;til.</p>\r\n	</li>\r\n	<li>\r\n	<p>Aglutinante en barras de cereal.</p>\r\n	</li>\r\n	<li>\r\n	<p>Fuente de az&uacute;cares fermentables.</p>\r\n	</li>\r\n</ul>\r\n', '<ul>\r\n	<li>\r\n	<p>Fuente de az&uacute;cares saludables, de metabolizaci&oacute;n diferenciada y f&aacute;cil absorci&oacute;n.</p>\r\n	</li>\r\n	<li>\r\n	<p>Importante fuente de energ&iacute;a</p>\r\n	</li>\r\n	<li>\r\n	<p>Contiene amino&aacute;cidos, vitaminas y minerales naturales provenientes de granos integrales</p>\r\n	</li>\r\n	<li>\r\n	<p>Bajo nivel de grasas y libre de grasas trans</p>\r\n	</li>\r\n	<li>\r\n	<p>Componentes antioxidantes</p>\r\n	</li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-03-23 21:46:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `calidad`
--

CREATE TABLE IF NOT EXISTS `calidad` (
`id` int(10) unsigned NOT NULL,
  `texto1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto2` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `calidad`
--

INSERT INTO `calidad` (`id`, `texto1`, `texto2`, `imagem`, `selos`, `created_at`, `updated_at`) VALUES
(1, '<p>Basada en las principales normas internacionales de gesti&oacute;n de calidad, Liot&eacute;cnica ha desarrollado un sistema &uacute;nico e integrado para el mejoramiento continuo de personas, productos, procesos y del ambiente, que se llama SGL - SISTEMA DE GESTI&Oacute;N LIOT&Eacute;CNICA.</p>\r\n\r\n<p>Atiende a las exigencias de:</p>\r\n\r\n<ul>\r\n	<li>OHSAS 18001</li>\r\n	<li>ISO 14000</li>\r\n	<li>ISO 17025</li>\r\n	<li>SA 8000</li>\r\n	<li>Integridad de los negocios</li>\r\n	<li>Programa 5S</li>\r\n</ul>\r\n', '<p>La&nbsp; f&aacute;brica&nbsp; de&nbsp; Extracto&nbsp; de&nbsp; Malta&nbsp; de&nbsp; Liot&eacute;cnica&nbsp; cuenta&nbsp; con&nbsp; la&nbsp; certificaci&oacute;n FSSC 22000 , garantizando la seguridad de sus productos por medio de un equipo t&eacute;cnico calificado y tambi&eacute;n mediante programas de entrenamiento de personas, con foco en la mejora continua de productos y procesos.</p>\r\n\r\n<p>El equipo de calidad supervisa y controla los procesos de producci&oacute;n, desde el recibimiento de las materias primas hasta la expedici&oacute;n del producto final, atendiendo a especificaciones t&eacute;cnicas, cumpliendo con todos los requisitos</p>\r\n\r\n<p>legales y de rastreabilidad. Un programa de muestreo de las producciones garantiza la conformidad de los ingredientes producidos en nuestras f&aacute;bricas.</p>\r\n\r\n<p>Nuestros&nbsp; proveedores&nbsp; son&nbsp; calificados&nbsp; y&nbsp; los&nbsp; procesos&nbsp; de&nbsp; producci&oacute;n&nbsp; son frecuentemente sometidos a auditor&iacute;as de Buenas Pr&aacute;cticas de Manufactura (BPM) y Estandarizaciones de procesos.</p>\r\n\r\n<p>Contamos con laboratorios internos de an&aacute;lisis sensorial, an&aacute;lisis microbiol&oacute;gicos y f&iacute;sico-qu&iacute;micos que son avalados todos los a&ntilde;os por el LGC - Standards Proficiency Tests (Reino Unido).</p>\r\n', '20150317155800_calidad-img.png', '20150325205219_calidad-selos.png', '0000-00-00 00:00:00', '2015-03-25 23:52:19');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
`id` int(10) unsigned NOT NULL,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contacto`
--

INSERT INTO `contacto` (`id`, `endereco`, `telefone`, `email`, `google_maps`, `created_at`, `updated_at`) VALUES
(1, 'Av. João Paulo I, 1098 - Embu - SP - Brasil - 06817-000', '+55 11 4785-2386 / 4785-2370', 'export@liotecnica.com.br', '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3655.1105683231144!2d-46.82748999999999!3d-23.636210999999985!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjPCsDM4JzEwLjQiUyA0NsKwNDknMzkuMCJX!5e0!3m2!1spt-BR!2sbr!4v1427316833064" width="100%" height="100%" frameborder="0" style="border:0"></iframe>', '0000-00-00 00:00:00', '2015-03-25 23:54:30');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contactos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contactos_recebidos` (
`id` int(10) unsigned NOT NULL,
  `prefijo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primer_nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apelido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo_postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `historia`
--

CREATE TABLE IF NOT EXISTS `historia` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `historia`
--

INSERT INTO `historia` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(3, '<p>Liot&eacute;cnica es una compa&ntilde;&iacute;a especializada en proveer las mejores soluciones en ingredientes. Acompa&ntilde;a las tendencias mundiales del mercado de alimentos, buscando siempre productos saludables, pr&aacute;cticos y convenientes. Actualmente, tiene 3 unidades de producci&oacute;n, ubicadas en el Estado de S&atilde;o Paulo, un importante punto log&iacute;stico y estrat&eacute;gico en Brasil.</p>\r\n\r\n<p>Desde el a&ntilde;o 1964, su compromiso con la calidad, las constantes inversiones en nuevas tecnolog&iacute;as y la voluntad de proporcionar lo mejor, han hecho que Liot&eacute;cnica se consolide como una compa&ntilde;&iacute;a s&oacute;lida y reconocida en el segmento de alimentos.</p>\r\n\r\n<p>Presente&nbsp; con&nbsp; su&nbsp; l&iacute;nea&nbsp; de&nbsp; ingredientes&nbsp; liofilizados&nbsp; en&nbsp; grandes&nbsp;<em>players </em>del mercado multinacional, la compa&ntilde;&iacute;a empieza a ofrecer una l&iacute;nea diversificada de Extractos de Malta, l&iacute;quidos y en polvo. El Extracto de Malta es un ingrediente vers&aacute;til, con beneficios sensoriales, tecnol&oacute;gicos y de gran importancia nutricional.</p>\r\n\r\n<h3>Liot&eacute;cnica: naturalmente, &iexcl;su mejor opci&oacute;n en ingredientes!</h3>\r\n', '20150316174931_img-nuestraHistoria-liotecnica.png', '0000-00-00 00:00:00', '2015-03-23 21:23:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `homepage`
--

CREATE TABLE IF NOT EXISTS `homepage` (
`id` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `homepage`
--

INSERT INTO `homepage` (`id`, `titulo`, `chamada1`, `chamada2`, `frase`, `created_at`, `updated_at`) VALUES
(3, 'Extracto de Malta', 'Más sabor y nutrición a sus productos', 'Malta es un ingrediente versátil, con beneficios sensoriales, tecnológicos y de gran importancia nutricional.', '.', '0000-00-00 00:00:00', '2015-03-25 23:39:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_03_13_135113_create_homepage_table', 1),
('2015_03_13_135128_create_historia_table', 1),
('2015_03_13_135144_create_productos_entrada_table', 1),
('2015_03_13_135155_create_productos_table', 2),
('2015_03_13_135210_create_aplicaciones_table', 2),
('2015_03_13_135225_create_beneficios_table', 2),
('2015_03_13_135245_create_procesos_table', 2),
('2015_03_13_135256_create_calidad_table', 2),
('2015_03_13_135312_create_contactos_recebidos_table', 2),
('2015_03_13_135322_create_contacto_table', 2),
('2015_03_13_141407_create_producto_aplicacion_table', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `procesos`
--

CREATE TABLE IF NOT EXISTS `procesos` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `procesos`
--

INSERT INTO `procesos` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Los granos son molidos y mezclados con agua, y bajo condiciones est&aacute;ndares de temperatura y tiempo, ocurre la hidr&oacute;lisis del almid&oacute;n y de las prote&iacute;nas, que se solubilizan en su fase l&iacute;quida, con las vitaminas y minerales que existen naturalmente en los granos.</p>\r\n\r\n<p>Despu&eacute;s, ocurre la filtraci&oacute;n, y ese alm&iacute;bar - que contiene los nutrientes solubles - es concentrado por medio de evaporaci&oacute;n al vac&iacute;o, resultando en un jarabe con 80% de s&oacute;lidos.</p>\r\n\r\n<p>Este jarabe es envasado y denominado extracto de malta l&iacute;quido.</p>\r\n\r\n<p>Para fabricar el extracto de malta en polvo, se realiza la deshidrataci&oacute;n al vac&iacute;o del jarabe y posteriormente el proceso de molienda y empaque.</p>\r\n', '0000-00-00 00:00:00', '2015-03-17 21:59:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
`id` int(10) unsigned NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ingredientes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `envase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `peso` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vida_util` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `humedad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kosher` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `actividad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `productos`
--

INSERT INTO `productos` (`id`, `tipo`, `titulo`, `imagem`, `ingredientes`, `envase`, `peso`, `vida_util`, `humedad`, `kosher`, `actividad`, `ordem`, `created_at`, `updated_at`) VALUES
(2, 'polvo', 'Extracto de Malta Dry Standard', '20150316184108_img-dry-standard.jpg', 'Cebada cruda y malteada', 'Saco de papel', '25 kg', '24 meses', 'Máx. 4,0', 'Disponible', 'No Diastásico', 0, '2015-03-16 21:41:08', '2015-03-23 21:41:59'),
(3, 'polvo', 'Extracto de Malta Dry Sweet', '20150316184140_img-dry-sweet.jpg', 'Cebada cruda y malteada', 'Saco de papel', '25 Kg', '24 meses', 'Máx. 4,0', 'Disponible', 'No Diastásico', 1, '2015-03-16 21:41:40', '2015-03-23 21:42:20'),
(4, 'polvo', 'Extracto de Malta Dry Toast', '20150316184151_img-dry-toast.jpg', 'Malta, cebada, hidróxido de potasio, lecitina de soja y colorante caramelo', 'Saco de papel', '25 Kg', '18 meses', 'Máx. 3,5', 'Disponible', 'No Diastásico', 2, '2015-03-16 21:41:51', '2015-03-23 21:42:45'),
(5, 'polvo', 'Extracto de Malta Dry Intense', '20150316184252_img-dry-intense.jpg', 'Cebada cruda y malteada', 'Saco de papel', '25 Kg', '18 meses', 'Máx. 4,0', 'Disponible', 'No Diastásico', 3, '2015-03-16 21:42:52', '2015-03-23 21:43:07'),
(6, 'polvo', 'Extracto de Malta Dry Medium', '20150316184303_img-dry-medium.jpg', 'Cebada malteada, cebada, color caramelo y lecitina de soja', 'Saco de papel', '25 Kg', '18 meses', 'Máx. 3,5', 'Disponible', 'No Diastásico', 4, '2015-03-16 21:43:03', '2015-03-23 21:43:34'),
(7, 'polvo', 'Extracto de Malta Dry Brew', '20150316184312_img-dry-brew.jpg', 'Cebada malteada', 'Saco de papel', '25 Kg', '18 meses', 'Máx. 4,0', '-', 'No Diastásico', 5, '2015-03-16 21:43:12', '2015-03-23 21:43:52'),
(8, 'liquido', 'Extracto de Malta Liquid Golden Light', '20150316184333_img-liquid-light.jpg', 'Cebada cruda y malteada', 'Tambor / Contenedor desechable', '280 y 300 kg / 1200kg', '12 meses', '78 - 82', 'Disponible', 'No Diastásico', 6, '2015-03-16 21:43:34', '2015-03-23 21:40:24'),
(9, 'liquido', 'Extracto de Malta Liquid Golden Medium', '20150316184350_img-liquid-medium.jpg', 'Cebada cruda y malteada', 'Tambor / Contenedor desechable', '280 y 300 kg / 1200 kg', '12 meses', '80 - 82', 'Disponible', 'No Diastásico', 7, '2015-03-16 21:43:50', '2015-03-23 21:40:12'),
(10, 'liquido', 'Extracto de Malta Liquid Golden Dark', '20150325204147_img-liquid-dark.jpg', 'Cebada malteada', 'Tambor / Contenedor desechable', '280 y 300 kg / 1200 kg', '12 meses', '78 - 82', '-', 'No Diastásico', 8, '2015-03-16 21:44:01', '2015-03-25 23:41:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `productos_entrada`
--

CREATE TABLE IF NOT EXISTS `productos_entrada` (
`id` int(10) unsigned NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `polvo_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `polvo_calorias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_carbohidratos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_fibras` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_proteina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_grasas_totales` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_grasas_trans` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `liquido_calorias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_carbohidratos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_fibras` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_proteina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_grasas_totales` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_grasas_trans` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `productos_entrada`
--

INSERT INTO `productos_entrada` (`id`, `texto`, `polvo_desc`, `polvo_calorias`, `polvo_carbohidratos`, `polvo_fibras`, `polvo_proteina`, `polvo_grasas_totales`, `polvo_grasas_trans`, `liquido_desc`, `liquido_calorias`, `liquido_carbohidratos`, `liquido_fibras`, `liquido_proteina`, `liquido_grasas_totales`, `liquido_grasas_trans`, `created_at`, `updated_at`) VALUES
(1, '<p>El Extracto de Malta es un ingrediente que resulta del proceso de maceraci&oacute;n de los granos integrales de cebada malteada y cebada, con el objetivo de promover la hidr&oacute;lisis del almid&oacute;n y de las prote&iacute;nas; para obtener un endulzante natural, con un perfil nutricional superior al del&nbsp;az&uacute;car convencional.</p>\r\n\r\n<p>La maltosa es el componente principal, aunque tambi&eacute;n contiene prote&iacute;nas hidrolizadas, vitaminas y minerales. Nuestro proceso es completamente automatizado y controlado, y no requiere la inclusi&oacute;n de aditivos, permitiendo de esta forma que los productos en los cuales el extracto de malta es aplicado, puedan contar el <em>claim </em>de naturalidad.</p>\r\n', '<p>Polvo fino<br />\r\nColor crema<br />\r\nSabor y aroma t&iacute;picos de malta<br />\r\n96% de s&oacute;lidos solubles<br />\r\nNo diast&aacute;sico</p>\r\n', '380 - 400', '88 - 93', '0', '2,0 - 7,0', '0,4 - 0,8', '0', '<p>L&iacute;quido denso y viscoso<br />\r\nColor dorado<br />\r\nSabor y aroma t&iacute;picos de malta<br />\r\n78 - 82% de s&oacute;lidos solubles<br />\r\nNo diast&aacute;sico</p>\r\n', '300 - 330', '70 - 78', '0', '1,6 - 5,2', '0,2 - 0,5', '0', '0000-00-00 00:00:00', '2015-03-25 23:38:57');

-- --------------------------------------------------------

--
-- Estrutura da tabela `producto_aplicacion`
--

CREATE TABLE IF NOT EXISTS `producto_aplicacion` (
`id` int(10) unsigned NOT NULL,
  `producto_id` int(11) NOT NULL,
  `aplicacion_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=50 ;

--
-- Extraindo dados da tabela `producto_aplicacion`
--

INSERT INTO `producto_aplicacion` (`id`, `producto_id`, `aplicacion_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 2, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 8, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 8, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 8, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 8, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 8, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 9, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 9, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 10, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 3, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 3, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 5, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 7, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id` int(10) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'contato@trupe.net', 'trupe', '$2y$10$bnyZztgEvP9Rn9i4dfLEVeKg0OK/f3OUW.siVeuBIN/d/S6CiaDOe', 'tiTMKZAQrfnTIJBGs4GFzLM4wgVwjFfLTazeJoGNk0pKVFiAPVpnYzt2QFw0', '0000-00-00 00:00:00', '2015-03-20 22:05:56'),
(5, 'contato@liotecnica.com.br', 'liotecnica', '$2y$10$Ds.d3wVAADwbqhoQ5t36MO23zXkUduFh5w9BGGsKxayGmSMUmnvKi', 'ApzkN0mTNwQFWq1PJ3cqGEQ71mZ4hv5J6PTVh4Zra3UucyBAVBcwwToqBwCu', '2015-03-20 22:05:52', '2015-03-20 22:37:35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aplicaciones`
--
ALTER TABLE `aplicaciones`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `beneficios`
--
ALTER TABLE `beneficios`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calidad`
--
ALTER TABLE `calidad`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacto`
--
ALTER TABLE `contacto`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactos_recebidos`
--
ALTER TABLE `contactos_recebidos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `historia`
--
ALTER TABLE `historia`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homepage`
--
ALTER TABLE `homepage`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procesos`
--
ALTER TABLE `procesos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productos`
--
ALTER TABLE `productos`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productos_entrada`
--
ALTER TABLE `productos_entrada`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `producto_aplicacion`
--
ALTER TABLE `producto_aplicacion`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `usuarios_email_unique` (`email`), ADD UNIQUE KEY `usuarios_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aplicaciones`
--
ALTER TABLE `aplicaciones`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `beneficios`
--
ALTER TABLE `beneficios`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `calidad`
--
ALTER TABLE `calidad`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contacto`
--
ALTER TABLE `contacto`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contactos_recebidos`
--
ALTER TABLE `contactos_recebidos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `historia`
--
ALTER TABLE `historia`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `homepage`
--
ALTER TABLE `homepage`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `procesos`
--
ALTER TABLE `procesos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `productos`
--
ALTER TABLE `productos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `productos_entrada`
--
ALTER TABLE `productos_entrada`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `producto_aplicacion`
--
ALTER TABLE `producto_aplicacion`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
