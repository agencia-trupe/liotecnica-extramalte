(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.toggleMobileNav = function(event) {
        event.preventDefault();

        var $handle = $(this),
            $nav    = $('header nav#mobile');

        $nav.slideToggle();
        $handle.toggleClass('close');
    };

    App.animateHomeImages = function() {
        setTimeout(function(){
            $('.graos').addClass('animate');
        }, 500);

        setTimeout(function(){
            $('.top h2').addClass('animate');
        }, 750);

        setTimeout(function(){
            $('.polvo, .top h3').addClass('animate');
        }, 1000);

        setTimeout(function(){
            $('.liquido, .bottom h3, .bottom p').addClass('animate');
        }, 1500);

        setTimeout(function(){
            $('.aplicacoes img').each(function(i) {
                $(this).delay((i++) * 300).fadeTo(200, 1);
            });
        }, 2000);
    };

    App.toggleProduct = function(event) {
        event.preventDefault();

        var target = $(event.target);
        if(target.hasClass('thumb-aplicacion') || target.parents('.thumb-aplicacion').length) {
            target.trigger('click');
            return false;
        }

        $(this).toggleClass('open').find('.thumb, .content').toggle();
        $('.productos').masonry('layout');
    };

    App.contactoSubmit = function(event) {
        event.preventDefault();

        var $formContato = $(this);
        var $formContatoResposta = $formContato.find('#form-resposta');

        $formContatoResposta.hide();

        $.post(BASE + '/contacto', {

            prefijo       : $('#prefijo').val(),
            primer_nombre : $('#primer_nombre').val(),
            apelido       : $('#apelido').val(),
            direccion1    : $('#direccion1').val(),
            direccion2    : $('#direccion2').val(),
            ciudad        : $('#ciudad').val(),
            estado        : $('#estado').val(),
            codigo_postal : $('#codigo_postal').val(),
            pais          : $('#pais').val(),
            empresa       : $('#empresa').val(),
            telefono      : $('#telefono').val(),
            email         : $('#email').val(),
            mensaje       : $('#mensaje').val()

        }, function(data) {

            if (data.status == 'success') $formContato[0].reset();

            $formContatoResposta.hide().text(data.message).fadeIn('slow');

        }, 'json');
    };

    App.showInfografico = function() {
        var $loading = $(this).find('.loading'),
            $content = $(this).find('.content');

        $loading.delay(1000).fadeOut('fast', function() {
            $content.fadeIn('slow');
        });
    };

    App.init = function() {
        $('#mobile-toggle').on('click touchstart', App.toggleMobileNav);

        $('#home').waitForImages(App.animateHomeImages);

        $('.infografico').waitForImages(App.showInfografico);

        $('#form-contacto').on('submit', App.contactoSubmit);

        var $productos = $('.productos');

        $productos.waitForImages(function() {
            $productos.masonry({
                itemSelector: '.item',
                columnWidth: '.grid-sizer',
                gutter: '.gutter-sizer'
            });
        });

        $productos.find('.item').on('click', App.toggleProduct);

        $('.thumb-aplicacion').fancybox({
            type: 'ajax',
            padding: 0,
            autoSize: false,
            scrolling: 'no',
            height: 'auto',
            helpers: {
                overlay: {
                    locked: true,
                    css: {
                        'background': 'rgba(207, 164, 85, .9)'
                    }
                }
            }
        });

        $('.nutricionales').fancybox({
            type: 'ajax',
            padding: 0,
            autoSize: false,
            scrolling: 'no',
            height: 'auto',
            width: '980px',
            minWidth: '980px',
            helpers: {
                overlay: {
                    locked: true,
                    css: {
                        'background': 'rgba(207, 164, 85, .9)'
                    }
                }
            }
        });
    };

    $(document).ready(App.init);

}(window, document, jQuery));