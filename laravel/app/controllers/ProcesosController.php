<?php

class ProcesosController extends BaseController {

    public function index()
    {
        $procesos = Procesos::first();

        return $this->view('frontend.procesos', compact('procesos'));
    }

}
