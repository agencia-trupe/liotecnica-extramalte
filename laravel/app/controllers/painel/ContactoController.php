<?php

namespace Painel;

use \Contacto, \Input, \Validator, \Redirect, \Session;

class ContactoController extends BasePainelController {

    private $validation_rules = [
        'endereco'    => 'required',
        'telefone'    => 'required',
        'email'       => 'required',
        'google_maps' => 'required'
    ];

    public function index()
    {
        $contacto = Contacto::first();

        return $this->view('painel.contacto.index', compact('contacto'));
    }

    public function update($id)
    {
        $contacto = Contacto::findOrFail($id);
        $input    = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $contacto->update($input);
            Session::flash('sucesso', 'Informações de contato alteradas com sucesso.');

            return Redirect::route('painel.contacto.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar informações de contato.'])
                ->withInput();

        }
    }

}