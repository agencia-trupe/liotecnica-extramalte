<?php

namespace Painel;

use \ContactoRecebido, \Session, \Redirect;

class ContactosRecebidosController extends BasePainelController {

    public function index()
    {
        $contactos_recebidos = ContactoRecebido::all();

        return $this->view('painel.contactosrecebidos.index', compact('contactos_recebidos'));
    }

    public function show($id)
    {
        $contacto = ContactoRecebido::findOrFail($id);
        $contacto->update(['lido' => 'true']);

        return $this->view('painel.contactosrecebidos.show', compact('contacto'));
    }

    public function destroy($id)
    {
        try {

            ContactoRecebido::destroy($id);
            Session::flash('sucesso', 'Mensagem removida com sucesso.');

            return Redirect::route('painel.contacto.recebidos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover mensagem.']);

        }
    }

}