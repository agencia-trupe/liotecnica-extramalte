<?php

namespace Painel;

use \Procesos, \Input, \Validator, \Redirect, \Session;

class ProcesosController extends BasePainelController {

    private $validation_rules = [
        'texto'    => 'required',
        'texto_en' => 'required'
    ];

    public function index()
    {
        $procesos = Procesos::first();

        return $this->view('painel.procesos.index', compact('procesos'));
    }

    public function update($id)
    {
        $procesos = Procesos::findOrFail($id);
        $input    = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $procesos->update($input);
            Session::flash('sucesso', 'Página alterada com sucesso.');

            return Redirect::route('painel.procesos.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar página.'])
                ->withInput();

        }
    }

}