<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHomepageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('homepage', function($table)
		{
		    $table->string('titulo_en')->after('titulo');
		    $table->string('chamada1_en')->after('chamada1');
		    $table->string('chamada2_en')->after('chamada2');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('homepage', function($table)
		{
		    $table->dropColumn(['titulo_en', 'chamada1_en', 'chamada2_en']);
		});
	}

}
