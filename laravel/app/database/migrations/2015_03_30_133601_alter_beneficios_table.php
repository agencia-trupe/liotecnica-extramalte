<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBeneficiosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('beneficios', function($table)
		{
			$table->text('sensoriales_en')->after('sensoriales');
			$table->text('tecnologicos_en')->after('tecnologicos');
			$table->text('nutricionales_en')->after('nutricionales');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('beneficios', function($table)
		{
			$table->dropColumn(['sensoriales_en', 'tecnologicos_en', 'nutricionales_en']);
		});
	}

}
