<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCalidadTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calidad', function($table)
		{
			$table->text('texto1_en')->after('texto1');
			$table->text('texto2_en')->after('texto2');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calidad', function($table)
		{
			$table->dropColumn(['texto1_en', 'texto2_en']);
		});
	}

}
