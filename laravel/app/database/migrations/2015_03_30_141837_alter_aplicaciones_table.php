<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAplicacionesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('aplicaciones', function($table)
		{
			$table->string('nome_en')->after('nome');
			$table->string('descricao_en')->after('descricao');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('aplicaciones', function($table)
		{
			$table->dropColumn(['nome_en', 'descricao_en']);
		});
	}

}
