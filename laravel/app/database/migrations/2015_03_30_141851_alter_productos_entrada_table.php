<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductosEntradaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productos_entrada', function($table)
		{
			$table->text('texto_en')->after('texto');
			$table->text('polvo_desc_en')->after('polvo_desc');
			$table->text('liquido_desc_en')->after('liquido_desc');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productos_entrada', function($table)
		{
			$table->dropColumn(['texto_en', 'polvo_desc_en', 'liquido_desc_en']);
		});
	}

}
