<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('productos', function($table)
		{
			$table->string('titulo_en')->after('titulo');
			$table->string('ingredientes_en')->after('ingredientes');
			$table->string('envase_en')->after('envase');
			$table->string('peso_en')->after('peso');
			$table->string('vida_util_en')->after('vida_util');
			$table->string('humedad_en')->after('humedad');
			$table->string('kosher_en')->after('kosher');
			$table->string('actividad_en')->after('actividad');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('productos', function($table)
		{
			$table->dropColumn(['titulo_en', 'ingredientes_en', 'envase_en', 'peso_en', 'vida_util_en', 'humedad_en', 'kosher_en', 'actividad_en']);
		});
	}

}
