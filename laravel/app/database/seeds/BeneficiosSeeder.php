<?php

class BeneficiosSeeder extends Seeder {

    public function run()
    {
        DB::table('beneficios')->delete();

        $data = array(
            array(
                'sensoriales' => '...',
                'tecnologicos' => '...',
                'nutricionales' => '...',
            )
        );

        DB::table('beneficios')->insert($data);
    }

}