<?php

class HistoriaSeeder extends Seeder {

    public function run()
    {
        DB::table('historia')->delete();

        $data = array(
            array(
                'texto' => '...',
                'imagem' => 'imagem.jpg',
            )
        );

        DB::table('historia')->insert($data);
    }

}