<?php

class ProcesosSeeder extends Seeder {

    public function run()
    {
        DB::table('procesos')->delete();

        $data = array(
            array(
                'texto' => '...',
            )
        );

        DB::table('procesos')->insert($data);
    }

}