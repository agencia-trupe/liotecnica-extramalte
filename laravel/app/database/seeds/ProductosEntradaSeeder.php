<?php

class ProductosEntradaSeeder extends Seeder {

    public function run()
    {
        DB::table('productos_entrada')->delete();

        $data = array(
            array(
                'texto' => '...',

                'polvo_desc' => '...',
                'polvo_calorias' => '380 - 400 kcal/100g',
                'polvo_carbohidratos' => '88 - 93%',
                'polvo_fibras' => '0%',
                'polvo_proteina' => '2,0 - 7,0%',
                'polvo_grasas_totales' => '0,4 - 0,8%',
                'polvo_grasas_trans' => '0%',

                'liquido_desc' => '...',
                'liquido_calorias' => '300 - 330 kcal/100g',
                'liquido_carbohidratos' => '70 - 78%',
                'liquido_fibras' => '0%',
                'liquido_proteina' => '1,6 - 5,2%',
                'liquido_grasas_totales' => '0,2 - 0,5%',
                'liquido_grasas_trans' => '0%',
            )
        );

        DB::table('productos_entrada')->insert($data);
    }

}