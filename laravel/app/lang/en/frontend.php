<?php

return array(

    'base' => [
        'titulo' => 'Malt Extract',

        'change_lang' => [
            'url'   => 'es',
            'title' => 'Versión en Español'
        ],

        'menu' => [
            'historia'   => 'our history',
            'productos'  => 'products',
            'beneficios' => 'benefits',
            'procesos'   => 'processes',
            'calidad'    => 'quality',
            'contacto'   => 'contact',
        ],

        'footer' => [
            'contato'   => 'To learn more about our malt extracts, please contact us:',
            'tel'       => 'Tel',
            'logo'      => 'Malt Extract is a product Liotécnica.',
            'copyright' => 'All rights reserved.',
            'trupe'     => 'Websites'
        ],

        'erro404' => 'Page not found'
    ],

    'home' => [
        'titulo'   => 'titulo_en',
        'chamada1' => 'chamada1_en',
        'chamada2' => 'chamada2_en'
    ],

    'historia' => [
        'titulo' => 'Our History',

        'texto'  => 'texto_en'
    ],

    'productos' => [
        'titulo'         => 'Products',
        'formatos'       => 'Presentation Formats: Liquid and Powder',
        'polvo'          => 'Powder',
        'liquido'        => 'Liquid',

        'polvo_desc'     => 'polvo_desc_en',
        'liquido_desc'   => 'liquido_desc_en',
        'texto'          => 'texto_en',

        'table'          => 'Typical Nutritional Composition',
        'th_polvo'       => 'Malt Extract Powder',
        'th_liquido'     => 'Malt Extract Liquid',
        'calorias'       => 'Calories (kcal)',
        'carbohidratos'  => 'Carbohydrates (g)',
        'fibras'         => 'Fibers (g)',
        'proteinas'      => 'Protein (g)',
        'grasas_totales' => 'Total Fat (g)',
        'grasas_trans'   => 'Trans Fat (g)',

        'titulo_item'    => 'titulo_en',
        'tipo_polvo'     => 'Malt Extract Powder',
        'tipo_liquido'   => 'Malt Extract Liquid',

        'ingredientes_t' => 'Ingredients',
        'envase_t'       => 'Package',
        'peso_t'         => 'Weight',
        'vida_util_t'    => 'Shelf life',
        'humedad_t'      => 'Humidity (%)',
        'kosher_t'       => 'Kosher Certificate',
        'actividad_t'    => 'Diastatic Activity',

        'ingredientes'   => 'ingredientes_en',
        'envase'         => 'envase_en',
        'peso'           => 'peso_en',
        'vida_util'      => 'vida_util_en',
        'humedad'        => 'humedad_en',
        'kosher'         => 'kosher_en',
        'actividad'      => 'actividad_en',

        'aplicaciones'   => 'Typical Applications'
    ],

    'beneficios' => [
        'titulo' => 'Benefits',

        'titulo_sensoriales'   => 'Sensorial Benefits:',
        'titulo_tecnologicos'  => 'Technological Benefits:',
        'titulo_nutricionales' => 'Nutritional Benefits:',

        'sensoriales'   => 'sensoriales_en',
        'tecnologicos'  => 'tecnologicos_en',
        'nutricionales' => 'nutricionales_en',
        'nome'          => 'nome_en',
        'descricao'     => 'descricao_en',

        'botao' => 'More Nutritional Benefits',

        'titulo_box' => 'Application Advantages',
    ],

    'procesos' => [
        'titulo' => 'Processes',

        'frase'  => 'Step By Step Of The Malt Extract Production Process',
        'texto'  => 'texto_en'
    ],

    'calidad' => [
        'titulo' => 'Quality',

        'texto1' => 'texto1_en',
        'texto2' => 'texto2_en'
    ],

    'contacto' => [
        'titulo'   => 'Contact',

        'info'     => 'To learn more about our products, please contact us:',
        'endereco' => 'Sales office and factories:',
        'email_i'  => 'Send us an email for more information.',

        'nombre'        => 'Full Name',
        'prefijo'       => 'Prefix',
        'primer_nombre' => 'First Name',
        'apelido'       => 'Nickname',
        'direccion'     => 'Address',
        'direccion1'    => 'Street address',
        'direccion2'    => 'Street address - Line 2',
        'ciudad'        => 'City',
        'estado'        => 'State/Province',
        'codigo_postal' => 'Postal/Zip Code',
        'pais'          => 'Country',
        'empresa'       => 'Company',
        'telefono'      => 'Phone Number',
        'email'         => 'E-mail',
        'email_exemplo' => 'myname@example.com',
        'mensaje'       => 'Message',
        'enviar'        => 'Submit',

        'fail'    => 'There are errors on the form. Please fix them before continuing.',
        'success' => 'Message sent with success!'
    ]

);
