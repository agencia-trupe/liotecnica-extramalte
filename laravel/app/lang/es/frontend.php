<?php

return array(

    'base' => [
        'titulo' => 'Extracto de Malta',

        'change_lang' => [
            'url'   => 'en',
            'title' => 'English Version'
        ],

        'menu' => [
            'historia'   => 'nuestra historia',
            'productos'  => 'productos',
            'beneficios' => 'beneficios',
            'procesos'   => 'procesos',
            'calidad'    => 'calidad',
            'contacto'   => 'contacto',
        ],

        'footer' => [
            'contato'   => 'Para saber más sobre nuestros extractos de malta, contáctenos:',
            'tel'       => 'Tel',
            'logo'      => 'Extracto de Malta es un producto Liotécnica.',
            'copyright' => 'Reservados todos los derechos.',
            'trupe'     => 'Creación de sitios web'
        ],

        'erro404' => 'Página no encontrada'
    ],

    'home' => [
        'titulo'   => 'titulo',
        'chamada1' => 'chamada1',
        'chamada2' => 'chamada2'
    ],

    'historia' => [
        'titulo' => 'Nuestra Historia',

        'texto'  => 'texto'
    ],

    'productos' => [
        'titulo'         => 'Productos',
        'formatos'       => 'Formatos de presentación: Polvo y Líquido',
        'polvo'          => 'Polvo',
        'liquido'        => 'Líquido',

        'polvo_desc'     => 'polvo_desc',
        'liquido_desc'   => 'liquido_desc',
        'texto'          => 'texto',

        'table'          => 'Composición Nutricional Típica',
        'th_polvo'       => 'Extracto de Malta en polvo',
        'th_liquido'     => 'Extracto de Malta líquido',
        'calorias'       => 'Calorias (kcal)',
        'carbohidratos'  => 'Carbohidratos (g)',
        'fibras'         => 'Fibras (g)',
        'proteinas'      => 'Proteínas (g)',
        'grasas_totales' => 'Grasas Totales (g)',
        'grasas_trans'   => 'Grasas Trans (g)',

        'titulo_item'    => 'titulo',
        'tipo_polvo'     => 'Extracto de Malta en polvo',
        'tipo_liquido'   => 'Extracto de Malta líquido',

        'ingredientes_t' => 'Ingredientes',
        'envase_t'       => 'Envase',
        'peso_t'         => 'Peso',
        'vida_util_t'    => 'Vida útil',
        'humedad_t'      => 'Humedad (%)',
        'kosher_t'       => 'Certificado Kosher',
        'actividad_t'    => 'Actividad Diastásica',

        'ingredientes'   => 'ingredientes',
        'envase'         => 'envase',
        'peso'           => 'peso',
        'vida_util'      => 'vida_util',
        'humedad'        => 'humedad',
        'kosher'         => 'kosher',
        'actividad'      => 'actividad',

        'aplicaciones'   => 'Aplicaciones Típicas'
    ],

    'beneficios' => [
        'titulo' => 'Beneficios',

        'titulo_sensoriales'   => 'Benefícios Sensoriales:',
        'titulo_tecnologicos'  => 'Benefícios Tecnológicos:',
        'titulo_nutricionales' => 'Benefícios Nutricionales:',

        'sensoriales'   => 'sensoriales',
        'tecnologicos'  => 'tecnologicos',
        'nutricionales' => 'nutricionales',
        'nome'          => 'nome',
        'descricao'     => 'descricao',

        'botao' => 'Más Beneficios Nutricionales',

        'titulo_box' => 'Ventajas en Aplicación',
    ],

    'procesos' => [
        'titulo' => 'Procesos',

        'frase'  => 'Etapas del proceso de Producción del Extracto de Malta',
        'texto'  => 'texto'
    ],

    'calidad' => [
        'titulo' => 'Calidad',

        'texto1' => 'texto1',
        'texto2' => 'texto2'
    ],

    'contacto' => [
        'titulo'   => 'Contacto',

        'info'     => 'Para saber más sobre nuestros productos, contáctenos:',
        'endereco' => 'Ubicación oficina y fábricas:',
        'email_i'  => 'Envíenos un correo electrónico para obtener más información.',

        'nombre'        => 'Nombre completo',
        'prefijo'       => 'Prefijo',
        'primer_nombre' => 'Primer Nombre',
        'apelido'       => 'Apelido',
        'direccion'     => 'Dirección',
        'direccion1'    => 'Dirección de la calle',
        'direccion2'    => 'Dirección de la calle - Línea 2',
        'ciudad'        => 'Ciudad',
        'estado'        => 'Estado/Província',
        'codigo_postal' => 'Código postal',
        'pais'          => 'País',
        'empresa'       => 'Empresa',
        'telefono'      => 'Número de teléfono',
        'email'         => 'Correo Electrónico',
        'email_exemplo' => 'minombre@ejemplo.com',
        'mensaje'       => 'Mensaje',
        'enviar'        => 'Enviar',

        'fail'    => 'Rellena todos los campos correctamente.',
        'success' => 'Mensaje enviado con suceso!'
    ]

);