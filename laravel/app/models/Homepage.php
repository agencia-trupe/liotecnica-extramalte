<?php

class Homepage extends Eloquent
{

    protected $table = 'homepage';

    protected $hidden = [];

    protected $guarded = ['id'];

}
