<?php

class ProductosEntrada extends Eloquent
{

    protected $table = 'productos_entrada';

    protected $hidden = [];

    protected $guarded = ['id'];

}
