<!DOCTYPE html>
<html>
<head>
    <title>[CONTACTO] {{ Config::get('projeto.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Prefijo:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $prefijo }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Primer Nombre:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $primer_nombre }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Apelido:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $apelido }}</span><br>
    <br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Dirección de la calle:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $direccion1 }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Dirección de la calle - Línea 2:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $direccion2 }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Ciudad:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $ciudad }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Estado/Província:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $estado }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Código postal:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $codigo_postal }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>País:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $pais }}</span><br>
    <br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Empresa:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Número de teléfono:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $telefono }}</span><br>
    <br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Correo Electrónico:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensaje:</span> <span style='font-size:14px;font-family:Verdana;'>{{ $mensaje }}</span><br>
</body>
</html>
