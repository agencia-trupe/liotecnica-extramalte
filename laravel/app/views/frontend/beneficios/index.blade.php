@section('content')

    <div id="title">
        <div class="center">
            <h2>@lang('frontend.beneficios.titulo')</h2>
        </div>
    </div>

    <main id="beneficios">
        <div class="center">
            <div class="textos">
                <div class="col">
                    <h2>@lang('frontend.beneficios.titulo_sensoriales')</h2>
                    {{ $beneficios->{Lang::get('frontend.beneficios.sensoriales')} }}
                </div>

                <div class="col">
                    <h2>@lang('frontend.beneficios.titulo_tecnologicos')</h2>
                    {{ $beneficios->{Lang::get('frontend.beneficios.tecnologicos')} }}
                </div>

                <div class="col">
                    <h2>@lang('frontend.beneficios.titulo_nutricionales')</h2>
                    {{ $beneficios->{Lang::get('frontend.beneficios.nutricionales')} }}
                    <a href="{{ route('beneficios.nutricionales') }}" class="nutricionales">@lang('frontend.beneficios.botao')</a>
                </div>
            </div>

            <div class="aplicaciones">
                <h6>@lang('frontend.beneficios.titulo_box')</h6>
                <div class="thumbs">
                    @foreach($aplicaciones as $aplicacion)
                        <a href="{{ route('beneficios.show', $aplicacion->id) }}" class="thumb-aplicacion">
                            <div class="imagem">
                                <img src="{{ url('assets/img/aplicaciones/thumb/'.$aplicacion->imagem) }}" alt="">
                                <div class="overlay"></div>
                            </div>
                            {{ $aplicacion->{Lang::get('frontend.beneficios.nome')} }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </main>

@stop