<div class="lightbox-nutricionales">
    <h1>Composición nutricional de Extracto de Malta comparativo con otros azúcares</h1>
    <p>Es posible evidenciar la superioridad de la composición nutricional del Extracto de Malta en comparación a otros azúcares o endulzantes.</p>

    <h3>COMPOSICIÓN NUTRICIONAL – EN 100g</h3>
  <table class="main">
        <thead>
            <th></th>
            <th>Extracto de Malta*</th>
            <th>% VD</th>
            <th>Azúcar Refinada**</th>
            <th>Azúcar Morena</th>
            <th>Miel**</th>
            <th>Jarabe de Maiz (HFCS)**</th>
        </thead>
        <tr>
            <td><p>Calorías  (kcal)</p></td>
            <td>390</td>
            <td></td>
            <td>389</td>
            <td>380</td>
            <td>304</td>
            <td>281</td>
        </tr>
        <tr>
            <td><p>Humedad (g)</p></td>
            <td>2</td>
            <td></td>
            <td>0,23</td>
            <td>1,34</td>
            <td>17,1</td>
            <td>24</td>
        </tr>
        <tr>
            <td><p>Proteína (g)</p></td>
            <td>5</td>
            <td></td>
            <td>&nbsp;</td>
            <td>0,12</td>
            <td>0,3</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><p>Cenizas (g)</p></td>
            <td>1,3</td>
            <td></td>
            <td>0,01</td>
            <td>0,45</td>
            <td>0,2</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><p>Carbohidratos (g)</p></td>
            <td>91</td>
            <td></td>
            <td>99,77</td>
            <td>98,09</td>
            <td>82,4</td>
            <td>76</td>
        </tr>
        <tr>
            <td class="mark-top mark-left"><p>Tiamina  (mg)</p></td>
            <td class="mark-top">0,23</td>
            <td class="mark-top mark-right">23</td>
            <td></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Niacina  (mg)</p></td>
            <td>6,8</td>
            <td class="mark-right">49</td>
            <td></td>
            <td>0,11</td>
            <td>0,12</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Riboflavina  (mg)</p></td>
            <td>0,11</td>
            <td class="mark-right">11</td>
            <td>0,01</td>
            <td>&nbsp;</td>
            <td>0,03</td>
            <td>0,01</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Piridoxina  (B6) (mg)</p></td>
            <td>0,18</td>
            <td class="mark-right">18</td>
            <td></td>
            <td>0,04</td>
            <td>0,02</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Hierro (mg)</p></td>
            <td>0,2</td>
            <td class="mark-right">2</td>
            <td>0,06</td>
            <td>0,71</td>
            <td>0,42</td>
            <td>0,03</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Calcio (mg)</p></td>
            <td>27</td>
            <td class="mark-right">3</td>
            <td>1</td>
            <td>83</td>
            <td>20</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Sodio (mg)</p></td>
            <td>40</td>
            <td class="mark-right">4</td>
            <td>2</td>
            <td>28</td>
            <td>4</td>
            <td>2</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Potasio (mg)</p></td>
            <td>430</td>
            <td class="mark-right">9</td>
            <td>2</td>
            <td>133</td>
            <td>52</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Magnesio (mg)</p></td>
            <td>80</td>
            <td class="mark-right">23</td>
            <td></td>
            <td>9</td>
            <td>2</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left mark-bottom"><p>Fósforo  (mg)</p></td>
            <td class="mark-bottom">155</td>
            <td class="mark-bottom mark-right">22</td>
            <td></td>
            <td>4</td>
            <td>4</td>
            <td>&nbsp;</td>
        </tr>
    </table>
  <h3>* Con base en Extracto de Malta Dry Standard. ** Fuente: USDA - National Nutrient Database for Standard Reference.</h3>

    <h1 style="margin-top:60px;">Composición Nutricional · Análisis de micronutrientes</h1>
    <div class="col">
        <h3>ANÁLISIS TÍPICO DE VITAMINAS<br>en 100g base seca</h3>
        <table>
            <tr>
                <td>Vitamina A (retinol) mcg</td>
                <td>< 21</td>
            </tr>
            <tr>
                <td>Vitamina B1 (tiamina) mg</td>
                <td>0,225</td>
            </tr>
            <tr>
                <td>Vitamina B2 (riboflavina) mg</td>
                <td>0,11</td>
            </tr>
            <tr>
                <td>Vitamina B3 (niacina) mg</td>
                <td>6,8</td>
            </tr>
            <tr>
                <td>Vitamina B5 (ác. pantoténico) mg</td>
                <td>0,44</td>
            </tr>
            <tr>
                <td>Vitamina B6 (piridoxina) mg</td>
                <td>0,183</td>
            </tr>
            <tr>
                <td>Vitamina B8 (biotina) mcg</td>
                <td>5,31</td>
            </tr>
            <tr>
                <td>Vitamina B9 (folato) mcg</td>
                <td>25</td>
            </tr>
            <tr>
                <td>Vitamina B12 (cianocobalamina) mcg</td>
                <td>0,171</td>
            </tr>
            <tr>
                <td>Vitamina C (total) mg</td>
                <td>< 1</td>
            </tr>
            <tr>
                <td>Vitamina E (alpha tocoferol) mg</td>
                <td>< 0,08</td>
            </tr>
            <tr>
                <td>Colina mg</td>
                <td>92,5</td>
            </tr>
            <tr>
                <td>Inositol mg</td>
                <td>14,8</td>
            </tr>
        </table>
    </div>

    <div class="col">
        <h3>ANÁLISIS TÍPICO DE MINERALES<br>mg / 100g base seca</h3>
        <table>
            <tr>
                <td>Fósforo</td>
                <td>155</td>
            </tr>
            <tr>
                <td>Potasio</td>
                <td>430</td>
            </tr>
            <tr>
                <td>Sodio</td>
                <td>40</td>
            </tr>
            <tr>
                <td>Calcio</td>
                <td>27</td>
            </tr>
            <tr>
                <td>Magnesio</td>
                <td>80</td>
            </tr>
            <tr>
                <td>Zinc</td>
                <td>0,08</td>
            </tr>
            <tr>
                <td>Hierro</td>
                <td>0,2</td>
            </tr>
            <tr>
                <td>Yodo</td>
                <td>< 0,02</td>
            </tr>
        </table>
    </div>

    <div class="col">
        <h3>VALORES RELATIVOS DE DULZURA<br>g / 100g base seca</h3>
        <table>
            <tr>
                <td>Sacarosa</td>
                <td>100</td>
            </tr>
            <tr>
                <td>Extracto de malta</td>
                <td>50-65</td>
            </tr>
            <tr>
                <td>Azúcar invertido</td>
                <td>90-100</td>
            </tr>
            <tr>
                <td>Glucosa de maíz</td>
                <td>65</td>
            </tr>
            <tr>
              <td>Miel</td>
              <td>100-110</td>
            </tr>
            <tr>
                <td>Jarabe de maíz (HFCS)</td>
                <td>117</td>
            </tr>
            <tr>
                <td colspan="2" class="omitidos"><p>Omitidos:  microbiología, pH, cenizas, etc.,  debido  a que se encuentran en fichas técnicas y varían de producto a producto.  También fue omitido el HPLC con perfil de  azúcar de Extracto de Malte Dry Standard</p></td>
            </tr>
        </table>
    </div>

    <div class="texto">
        <p>El  extracto de malta, a diferencia de otros azúcares contiene vitaminas del  complejo B, especialmente las vitaminas B1, B2, B3 y B6. Estas vitaminas son reconociadas  por su importancia para el buen funcionamiento del sistema nervioso, digestivo,  y por contribuír  a la visión y la salud de la piel. Además, el extrato  de malta contiene cantidades significativas de magnesio, un mineral importante  para el correcto funcionamento  del sistema cardiovascular.</p></div>
</div>