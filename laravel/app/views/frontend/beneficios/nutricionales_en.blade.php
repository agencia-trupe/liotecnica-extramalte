<div class="lightbox-nutricionales">
    <h1>Nutritional composition of malt extract compared with other sugars</h1>
    <p>It is possible to demonstrate the superiority of the nutritional composition of Malt Extract compared to other sugars or sweeteners.</p>

    <h3>NUTRITIONAL COMPOSITION – 100g</h3>
  <table class="main">
        <thead>
            <th></th>
            <th>Malt Extract*</th>
            <th>% RDI</th>
            <th>Refined Sugar**</th>
            <th>Brown Sugar**</th>
            <th>Honey**</th>
            <th>Glucose Syrup**</th>
        </thead>
        <tr>
            <td><p>Calories (kcal)</p></td>
            <td>390</td>
            <td></td>
            <td>389</td>
            <td>380</td>
            <td>304</td>
            <td>281</td>
        </tr>
        <tr>
            <td><p>Humidity (g)</p></td>
            <td>2</td>
            <td></td>
            <td>0,23</td>
            <td>1,34</td>
            <td>17,1</td>
            <td>24</td>
        </tr>
        <tr>
            <td><p>Protein (g)</p></td>
            <td>5</td>
            <td></td>
            <td>&nbsp;</td>
            <td>0,12</td>
            <td>0,3</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><p>Ash (g)</p></td>
            <td>1,3</td>
            <td></td>
            <td>0,01</td>
            <td>0,45</td>
            <td>0,2</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><p>Carbohydrates (g)</p></td>
            <td>91</td>
            <td></td>
            <td>99,77</td>
            <td>98,09</td>
            <td>82,4</td>
            <td>76</td>
        </tr>
        <tr>
            <td class="mark-top mark-left"><p>Thiamine (mg)</p></td>
            <td class="mark-top">0,23</td>
            <td class="mark-top mark-right">23</td>
            <td></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Niacin (mg)</p></td>
            <td>6,8</td>
            <td class="mark-right">49</td>
            <td></td>
            <td>0,11</td>
            <td>0,12</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Riboflavin (mg)</p></td>
            <td>0,11</td>
            <td class="mark-right">11</td>
            <td>0,01</td>
            <td>&nbsp;</td>
            <td>0,03</td>
            <td>0,01</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Pyridoxine (B6) (mg)</p></td>
            <td>0,18</td>
            <td class="mark-right">18</td>
            <td></td>
            <td>0,04</td>
            <td>0,02</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Iron (mg)</p></td>
            <td>0,2</td>
            <td class="mark-right">2</td>
            <td>0,06</td>
            <td>0,71</td>
            <td>0,42</td>
            <td>0,03</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Calcium (mg)</p></td>
            <td>27</td>
            <td class="mark-right">3</td>
            <td>1</td>
            <td>83</td>
            <td>20</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Sodium (mg)</p></td>
            <td>40</td>
            <td class="mark-right">4</td>
            <td>2</td>
            <td>28</td>
            <td>4</td>
            <td>2</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Potassium (mg)</p></td>
            <td>430</td>
            <td class="mark-right">9</td>
            <td>2</td>
            <td>133</td>
            <td>52</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left"><p>Magnesium (mg)</p></td>
            <td>80</td>
            <td class="mark-right">23</td>
            <td></td>
            <td>9</td>
            <td>2</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="mark-left mark-bottom"><p>Phosphorus (mg)</p></td>
            <td class="mark-bottom">155</td>
            <td class="mark-bottom mark-right">22</td>
            <td></td>
            <td>4</td>
            <td>4</td>
            <td>&nbsp;</td>
        </tr>
    </table>
  <h3>* Based on Malt Extract Dry Standard. ** References: USDA - National Nutrient Database for Standard Reference.</h3>

    <h1 style="margin-top:60px;">Nutritional Composition · Micronutrient Analysis</h1>
    <div class="col">
        <h3>TYPICAL VITAMINS ANALYSIS<br>g / 100g dry base</h3>
        <table>
            <tr>
                <td>Vit A (retinol) mcg</td>
                <td>< 21</td>
            </tr>
            <tr>
                <td>Vit B1 (thiamin) mg</td>
                <td>0,225</td>
            </tr>
            <tr>
                <td>Vit B2 (riboflavin) mg</td>
                <td>0,11</td>
            </tr>
            <tr>
                <td>Vit B3 (niacin) mg</td>
                <td>6,8</td>
            </tr>
            <tr>
                <td>Vit B5 (pantothenic acid) mg</td>
                <td>0,44</td>
            </tr>
            <tr>
                <td>Vit B6 (pyridoxine) mg</td>
                <td>0,183</td>
            </tr>
            <tr>
                <td>Vit B8 (biotin) mcg</td>
                <td>5,31</td>
            </tr>
            <tr>
                <td>Vit B9 (folate) mcg</td>
                <td>25</td>
            </tr>
            <tr>
                <td>Vit B12 (cyanocobalamine) mcg</td>
                <td>0,171</td>
            </tr>
            <tr>
                <td>Vit C (total) mg</td>
                <td>< 1</td>
            </tr>
            <tr>
                <td>Vit E (alpha tocopherol) mg</td>
                <td>< 0,08</td>
            </tr>
            <tr>
                <td>Choline mg</td>
                <td>92,5</td>
            </tr>
            <tr>
                <td>Inositol mg</td>
                <td>14,8</td>
            </tr>
        </table>
    </div>

    <div class="col">
        <h3>TYPICAL MINERALS ANALYSIS<br>mg / 100g dry base</h3>
        <table>
            <tr>
                <td>Phosphorus</td>
                <td>155</td>
            </tr>
            <tr>
                <td>Potassium</td>
                <td>430</td>
            </tr>
            <tr>
                <td>Sodium</td>
                <td>40</td>
            </tr>
            <tr>
                <td>Calcium</td>
                <td>27</td>
            </tr>
            <tr>
                <td>Magnesium</td>
                <td>80</td>
            </tr>
            <tr>
                <td>Zinc</td>
                <td>0,08</td>
            </tr>
            <tr>
                <td>Iron</td>
                <td>0,2</td>
            </tr>
            <tr>
                <td>Iodine</td>
                <td>< 0,02</td>
            </tr>
        </table>
    </div>

    <div class="col">
        <h3>RELATIVE SWEETNESS<br>g / 100g dry base</h3>
        <table>
            <tr>
                <td>Sucrose</td>
                <td>100</td>
            </tr>
            <tr>
                <td>Malt Extract</td>
                <td>50-65</td>
            </tr>
            <tr>
                <td>Invert Sugar</td>
                <td>90-100</td>
            </tr>
            <tr>
                <td>Corn Glucose</td>
                <td>65</td>
            </tr>
            <tr>
              <td>Honey</td>
              <td>100-110</td>
            </tr>
            <tr>
                <td>Fructose</td>
                <td>117</td>
            </tr>
            <tr>
                <td colspan="2" class="omitidos"><p>Omitted: microbiology, pH, ashes, etc.; they are included in the specification sheet and vary from product to product. HPLC with EMSA sugar profile was also omitted.</p></td>
            </tr>
        </table>
    </div>

    <div class="texto">
        <p>The Malt Extract, unlike other sugars, contains B complex vitamins, especially B1, B2, B3 and B6. These vitamins are recognized for their importance to the proper functioning of nervous and digestive system, vision and skin health. Moreover, malt extract contains significant amount of magnesium, important mineral for the proper functioning of the cardiovascular system.</p></div>
</div>
