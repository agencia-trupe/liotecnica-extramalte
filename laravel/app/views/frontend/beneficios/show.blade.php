<div class="lightbox-beneficio">
    <div class="imagem">
        <img src="{{ url('assets/img/aplicaciones/'.$beneficio->imagem) }}" alt="">
    </div>
    <div class="texto">
        <h2>@lang('frontend.beneficios.titulo')</h2>
        <h1>{{ $beneficio->{Lang::get('frontend.beneficios.nome')} }}</h1>
        {{ $beneficio->{Lang::get('frontend.beneficios.descricao')} }}
    </div>
</div>