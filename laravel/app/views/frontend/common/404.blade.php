@extends('frontend.common.template')
@section('content')

    <main class="not-found">
        <div class="center">
            <h1 style="text-align:center;">@lang('frontend.base.erro404')</h1>
        </div>
    </main>

@stop