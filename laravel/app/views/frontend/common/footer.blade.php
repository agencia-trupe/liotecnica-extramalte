@if(!str_is('home', Route::currentRouteName()))
    <div class="footer-bg">
        <div class="center">@lang('frontend.base.titulo')</div>
    </div>
@endif
    <footer>
        <div class="center">
            <nav>
                <p>@lang('frontend.base.titulo')</p>
                <a href="{{ route('home') }}">home</a>
                <a href="{{ route('historia') }}">@lang('frontend.base.menu.historia')</a>
                <a href="{{ route('productos') }}">@lang('frontend.base.menu.productos')</a>
            </nav>
            <nav>
                <a href="{{ route('beneficios') }}">@lang('frontend.base.menu.beneficios')</a>
                <a href="{{ route('procesos') }}">@lang('frontend.base.menu.procesos')</a>
                <a href="{{ route('calidad') }}">@lang('frontend.base.menu.calidad')</a>
                <a href="{{ route('contacto') }}">@lang('frontend.base.menu.contacto')</a>
            </nav>

            <div class="contacto">
                <p>@lang('frontend.base.footer.contato')</p>
                <p class="tel">@lang('frontend.base.footer.tel'): {{ $contacto->telefone }}</p>
                <a href="mailto:{{ $contacto->email }}">{{ $contacto->email }}</a>
            </div>

            <div class="logo">
                <img src="{{ url('assets/img/layout/liotecnica-footer.png') }}" alt="">
                <p>@lang('frontend.base.footer.logo')</p>
                <a href="http://www.liotecnica.com.br">www.liotecnica.com.br</a>
            </div>

            <div class="copyright">
                <p>© {{ date('Y') }} {{ Config::get('projeto.name') }} - @lang('frontend.base.footer.copyright')</p>
                <p><a href="http://trupe.net" target="_blank">@lang('frontend.base.footer.trupe')</a>: <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>.</p>
            </div>
        </div>
    </footer>