    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>
            <nav id="desktop">
                <a href="{{ route('home') }}"@if(str_is('home', Route::currentRouteName())) class="active"@endif>home</a>
                <a href="{{ route('historia') }}"@if(str_is('historia', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.historia')</a>
                <a href="{{ route('productos') }}"@if(str_is('productos', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.productos')</a>
                <a href="{{ route('beneficios') }}"@if(str_is('beneficios', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.beneficios')</a>
                <a href="{{ route('procesos') }}"@if(str_is('procesos', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.procesos')</a>
                <a href="{{ route('calidad') }}"@if(str_is('calidad', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.calidad')</a>
                <a href="{{ route('contacto') }}"@if(str_is('contacto', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.contacto')</a>
                <a href="{{ route('idioma', Lang::get('frontend.base.change_lang.url')) }}" title="@lang('frontend.base.change_lang.title')" class="idioma @lang('frontend.base.change_lang.url')">@lang('frontend.base.change_lang.title')</a>
            </nav>
        </div>

        <nav id="mobile">
            <a href="{{ route('home') }}"@if(str_is('home', Route::currentRouteName())) class="active"@endif>home</a>
            <a href="{{ route('historia') }}"@if(str_is('historia', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.historia')</a>
            <a href="{{ route('productos') }}"@if(str_is('productos', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.productos')</a>
            <a href="{{ route('beneficios') }}"@if(str_is('beneficios', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.beneficios')</a>
            <a href="{{ route('procesos') }}"@if(str_is('procesos', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.procesos')</a>
            <a href="{{ route('calidad') }}"@if(str_is('calidad', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.calidad')</a>
            <a href="{{ route('contacto') }}"@if(str_is('contacto', Route::currentRouteName())) class="active"@endif>@lang('frontend.base.menu.contacto')</a>
            <a href="{{ route('idioma', Lang::get('frontend.base.change_lang.url')) }}" title="@lang('frontend.base.change_lang.title')" class="idioma @lang('frontend.base.change_lang.url')">@lang('frontend.base.change_lang.title')</a>
        </nav>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>
