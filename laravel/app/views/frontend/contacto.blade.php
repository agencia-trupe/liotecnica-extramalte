@section('content')

    <div id="title">
        <div class="center">
            <h2>@lang('frontend.contacto.titulo')</h2>
        </div>
    </div>

    <main id="contacto">
        <div class="center">
            <div class="info">
                <h3>@lang('frontend.contacto.info')</h3>
                <p>@lang('frontend.contacto.endereco')</p>
                <p>{{ $contacto->endereco }}</p>
                <p>@lang('frontend.base.footer.tel'): {{ $contacto->telefone }}</p>
                <a href="mailto:{{ $contacto->email }}">{{ $contacto->email }}</a>
                <h4>@lang('frontend.contacto.email_i')</h4>

                <div class="googlemaps">
                    {{ $contacto->google_maps }}
                </div>
            </div>

            <form action="" method="post" id="form-contacto">
                <label for="prefijo">@lang('frontend.contacto.nombre')*</label>
                <input type="text" name="prefijo" id="prefijo" placeholder="@lang('frontend.contacto.prefijo')" required>
                <input type="text" name="primer_nombre" id="primer_nombre" placeholder="@lang('frontend.contacto.primer_nombre')" required>
                <input type="text" name="apelido" id="apelido" placeholder="@lang('frontend.contacto.apelido')">

                <label for="direccion1">@lang('frontend.contacto.direccion')*</label>
                <input type="text" name="direccion1" id="direccion1" placeholder="@lang('frontend.contacto.direccion1')" required>
                <input type="text" name="direccion2" id="direccion2" placeholder="@lang('frontend.contacto.direccion2')">
                <input type="text" name="ciudad" id="ciudad" placeholder="@lang('frontend.contacto.ciudad')" required>
                <input type="text" name="estado" id="estado" placeholder="@lang('frontend.contacto.estado')" required>
                <input type="text" name="codigo_postal" id="codigo_postal" placeholder="@lang('frontend.contacto.codigo_postal')" required>
                <select name="pais" id="pais" required>
                    <option value="" disabled selected>@lang('frontend.contacto.pais')</option>
                    @foreach($contacto->paises as $pais)
                    <option value="{{ $pais }}">{{ $pais }}</option>
                    @endforeach
                </select>

                <label for="empresa">@lang('frontend.contacto.empresa')*</label>
                <input type="text" name="empresa" id="empresa" placeholder="@lang('frontend.contacto.empresa')" required>
                <input type="text" name="telefono" id="telefono" placeholder="@lang('frontend.contacto.telefono')" required>

                <label for="email">@lang('frontend.contacto.email')*</label>
                <input type="email" name="email" id="email" placeholder="@lang('frontend.contacto.email_exemplo')" required>

                <label for="mensaje">@lang('frontend.contacto.mensaje')*</label>
                <textarea name="mensaje" id="mensaje" placeholder="@lang('frontend.contacto.mensaje')" required></textarea>

                <input type="submit" value="@lang('frontend.contacto.enviar')">
                <div class="resposta-wrapper">
                    <div id="form-resposta"></div>
                </div>
            </form>
        </div>
    </main>

@stop