@section('content')

    <div id="title">
        <div class="center">
            <h2>@lang('frontend.historia.titulo')</h2>
        </div>
    </div>

    <main id="historia">
        <div class="center">
            <div class="texto">
                {{ $historia->{Lang::get('frontend.historia.texto')} }}
            </div>
            <div class="imagem">
                <img src="{{ url('assets/img/historia/'.$historia->imagem) }}" alt="">
            </div>
        </div>
    </main>

@stop