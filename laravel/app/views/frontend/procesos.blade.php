@section('content')

    <div id="title">
        <div class="center">
            <h2>@lang('frontend.procesos.titulo')</h2>
        </div>
    </div>

    <main id="procesos">
        <div class="center">
            <div class="texto">
                <h3>@lang('frontend.procesos.frase')</h3>
                {{ $procesos->{Lang::get('frontend.procesos.texto')} }}
            </div>

            <div class="infografico">
                <div class="loading"></div>
                <div class="content">
                    <div class="wrapper">
                        <div class="col">
                            <img src="{{ url('assets/img/layout/procesos/'.App::getLocale().'/a1.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/'.App::getLocale().'/a3.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/'.App::getLocale().'/a5.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/'.App::getLocale().'/b2.gif') }}" alt="">
                        </div>
                        <div class="col">
                            <img src="{{ url('assets/img/layout/procesos/'.App::getLocale().'/a2.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/'.App::getLocale().'/a4.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/'.App::getLocale().'/b1.gif') }}" alt="">
                            <img src="{{ url('assets/img/layout/procesos/'.App::getLocale().'/b3.gif') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@stop