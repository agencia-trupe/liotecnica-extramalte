@section('content')

    <div id="title">
        <div class="center">
            <h2>@lang('frontend.productos.titulo')</h2>
        </div>
    </div>

    <main id="productos">
        <div class="entrada center">
            <div class="left">
                <h3>@lang('frontend.base.titulo')</h3>
                <h4>@lang('frontend.productos.formatos')</h4>
                <div class="formato polvo">
                    <h5>@lang('frontend.productos.polvo')</h5>
                    {{ $entrada->{Lang::get('frontend.productos.polvo_desc')} }}
                    <img src="{{ url('assets/img/layout/productos-polvo.png') }}" alt="">
                </div>
                <div class="formato liquido">
                    <h5>@lang('frontend.productos.liquido')</h5>
                    {{ $entrada->{Lang::get('frontend.productos.liquido_desc')} }}
                    <img src="{{ url('assets/img/layout/productos-liquido.png') }}" alt="">
                </div>
            </div>

            <div class="right">
                <div class="texto">
                    {{ $entrada->{Lang::get('frontend.productos.texto')} }}
                </div>
                <div class="tabela">
                    <h4>@lang('frontend.productos.table')</h4>
                    <table>
                        <thead>
                            <th></th>
                            <th>@lang('frontend.productos.th_polvo')</th>
                            <th>@lang('frontend.productos.th_liquido')</th>
                        </thead>
                        <tr>
                            <td>@lang('frontend.productos.calorias')</td>
                            <td>{{ $entrada->polvo_calorias }}</td>
                            <td>{{ $entrada->liquido_calorias }}</td>
                        </tr>
                        <tr>
                            <td>@lang('frontend.productos.carbohidratos')</td>
                            <td>{{ $entrada->polvo_carbohidratos }}</td>
                            <td>{{ $entrada->liquido_carbohidratos }}</td>
                        </tr>
                        <tr>
                            <td>@lang('frontend.productos.fibras')</td>
                            <td>{{ $entrada->polvo_fibras }}</td>
                            <td>{{ $entrada->liquido_fibras }}</td>
                        </tr>
                        <tr>
                            <td>@lang('frontend.productos.proteinas')</td>
                            <td>{{ $entrada->polvo_proteina }}</td>
                            <td>{{ $entrada->liquido_proteina }}</td>
                        </tr>
                        <tr>
                            <td>@lang('frontend.productos.grasas_totales')</td>
                            <td>{{ $entrada->polvo_grasas_totales }}</td>
                            <td>{{ $entrada->liquido_grasas_totales }}</td>
                        </tr>
                        <tr>
                            <td>@lang('frontend.productos.grasas_trans')</td>
                            <td>{{ $entrada->polvo_grasas_trans }}</td>
                            <td>{{ $entrada->liquido_grasas_trans }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="productos center">
            <div class="gutter-sizer"></div>
            <div class="grid-sizer"></div>
@if(count($productos))
@foreach($productos as $producto)
            <div class="item">
                <div class="thumb">
                    <div class="imagem">
                        <img src="{{ url('assets/img/productos/'.$producto->imagem) }}" alt="">
                    </div>
                    <h5>{{ $producto->{Lang::get('frontend.productos.titulo_item')} }}</h5>
                </div>
                <div class="content">
                    <div class="desc">
                        <div class="imagem">
                            <img src="{{ url('assets/img/productos/'.$producto->imagem) }}" alt="">
                        </div>

                        <div class="titulo">
                            <h5>{{ $producto->{Lang::get('frontend.productos.titulo_item')} }}</h5>
                            <h6>{{ Lang::get('frontend.productos.tipo_'.($producto->tipo == 'polvo' ? 'polvo' : 'liquido')) }}</h6>
                        </div>

                        <table>
                            <tr>
                                <td class="title" nowrap>@lang('frontend.productos.ingredientes_t')</td>
                                <td>{{ $producto->{Lang::get('frontend.productos.ingredientes')} }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>@lang('frontend.productos.envase_t')</td>
                                <td>{{ $producto->{Lang::get('frontend.productos.envase')} }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>@lang('frontend.productos.peso_t')</td>
                                <td>{{ $producto->{Lang::get('frontend.productos.peso')} }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>@lang('frontend.productos.vida_util_t')</td>
                                <td>{{ $producto->{Lang::get('frontend.productos.vida_util')} }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>@lang('frontend.productos.humedad_t')</td>
                                <td>{{ $producto->{Lang::get('frontend.productos.humedad')} }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>@lang('frontend.productos.kosher_t')</td>
                                <td>{{ $producto->{Lang::get('frontend.productos.kosher')} }}</td>
                            </tr>
                            <tr>
                                <td class="title" nowrap>@lang('frontend.productos.actividad_t')</td>
                                <td>{{ $producto->{Lang::get('frontend.productos.actividad')} }}</td>
                            </tr>
                        </table>
                    </div>

                    <div class="aplicaciones">
                        <h6>@lang('frontend.productos.aplicaciones')</h6>
                        <div class="thumbs">
                            @foreach($producto->aplicaciones as $aplicacion)
                                <a href="{{ route('beneficios.show', $aplicacion->id) }}" class="thumb-aplicacion">
                                    <div class="imagem">
                                        <img src="{{ url('assets/img/aplicaciones/thumb/'.$aplicacion->imagem) }}" alt="">
                                        <div class="overlay"></div>
                                    </div>
                                    {{ $aplicacion->{Lang::get('frontend.beneficios.nome')} }}
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
@endforeach
@endif
        </div>
    </main>

@stop