@section('content')

    <legend>
        <h2>Adicionar Aplicação</h2>
    </legend>

    {{ Form::open(['route' => 'painel.aplicaciones.store', 'files' => true]) }}

        @include('painel.aplicaciones._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop