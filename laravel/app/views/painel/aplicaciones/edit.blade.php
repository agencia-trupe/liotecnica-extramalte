@section('content')

    <legend>
        <h2>Editar Aplicação</h2>
    </legend>

    {{ Form::model($aplicacion, [
        'route' => ['painel.aplicaciones.update', $aplicacion->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.aplicaciones._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop