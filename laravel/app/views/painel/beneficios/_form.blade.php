@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('sensoriales', 'Sensoriales') }}
        {{ Form::textarea('sensoriales', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('sensoriales_en', 'Sensoriales [INGLÊS]') }}
        {{ Form::textarea('sensoriales_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('tecnologicos', 'Tecnológicos') }}
        {{ Form::textarea('tecnologicos', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('tecnologicos_en', 'Tecnológicos [INGLÊS]') }}
        {{ Form::textarea('tecnologicos_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('nutricionales', 'Nutricionales') }}
        {{ Form::textarea('nutricionales', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('nutricionales_en', 'Nutricionales [INGLÊS]') }}
        {{ Form::textarea('nutricionales_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'beneficios']) }}
    </div>
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}