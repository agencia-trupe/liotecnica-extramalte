@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('texto1', 'Texto 1') }}
        {{ Form::textarea('texto1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'calidad']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('texto1_en', 'Texto 1 [INGLÊS]') }}
        {{ Form::textarea('texto1_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'calidad']) }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('texto2', 'Texto 2') }}
        {{ Form::textarea('texto2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'calidad']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('texto2_en', 'Texto 2 [INGLÊS]') }}
        {{ Form::textarea('texto2_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'calidad']) }}
    </div>
</div>

<div class="well form-group">
    {{ Form::label('imagem', 'Alterar imagem') }}
    <img src="{{ url('assets/img/calidad/'.$calidad->imagem) }}" style="display:block;margin-bottom:10px;" class="img-responsive">
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ Form::label('selos', 'Alterar selos') }}
    <img src="{{ url('assets/img/calidad/'.$calidad->selos) }}" style="display:block;margin-bottom:10px;" class="img-responsive">
    {{ Form::file('selos', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}