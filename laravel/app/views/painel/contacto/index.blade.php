@section('content')

    <legend>
        <h2>Informações de Contato</h2>
    </legend>

    {{ Form::model($contacto, [
        'route' => ['painel.contacto.update', $contacto->id],
        'method' => 'patch'])
    }}

        @include('painel.contacto._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop