@section('content')

    <legend>
        <h2>Contato Recebido</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contacto->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Prefijo</label>
        <div class="well">{{ $contacto->prefijo }}</div>
    </div>

    <div class="form-group">
        <label>Primer Nombre</label>
        <div class="well">{{ $contacto->primer_nombre }}</div>
    </div>

    <div class="form-group">
        <label>Apelido</label>
        <div class="well">{{ $contacto->apelido }}</div>
    </div>

    <div class="form-group">
        <label>Dirección de la calle</label>
        <div class="well">{{ $contacto->direccion1 }}</div>
    </div>

    <div class="form-group">
        <label>Dirección de la calle - Linea 2</label>
        <div class="well">{{ $contacto->direccion2 }}</div>
    </div>

    <div class="form-group">
        <label>Ciudad</label>
        <div class="well">{{ $contacto->ciudad }}</div>
    </div>

    <div class="form-group">
        <label>Estado / Provincia</label>
        <div class="well">{{ $contacto->estado }}</div>
    </div>

    <div class="form-group">
        <label>Código Postal</label>
        <div class="well">{{ $contacto->codigo_postal }}</div>
    </div>

    <div class="form-group">
        <label>País</label>
        <div class="well">{{ $contacto->pais }}</div>
    </div>

    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $contacto->empresa }}</div>
    </div>

    <div class="form-group">
        <label>Número de teléfono</label>
        <div class="well">{{ $contacto->telefono }}</div>
    </div>

    <div class="form-group">
        <label>Correo Electrónico</label>
        <div class="well">{{ $contacto->email }}</div>
    </div>

    <div class="form-group">
        <label>Mensaje</label>
        <div class="well">{{ $contacto->mensaje }}</div>
    </div>

    <a href="{{ route('painel.contacto.recebidos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop