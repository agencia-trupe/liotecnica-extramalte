@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('titulo', 'Título') }}
        {{ Form::text('titulo', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('titulo_en', 'Título [INGLÊS]') }}
        {{ Form::text('titulo_en', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('chamada1', 'Chamada 1') }}
        {{ Form::text('chamada1', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('chamada1_en', 'Chamada 1 [INGLÊS]') }}
        {{ Form::text('chamada1_en', null, ['class' => 'form-control']) }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('chamada2', 'Chamada 2') }}
        {{ Form::text('chamada2', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('chamada2_en', 'Chamada 2 [INGLÊS]') }}
        {{ Form::text('chamada2_en', null, ['class' => 'form-control']) }}
    </div>
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}