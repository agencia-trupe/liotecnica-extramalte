@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('texto', 'Texto') }}
        {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'procesos']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('texto_en', 'Texto [INGLÊS]') }}
        {{ Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'procesos']) }}
    </div>
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}