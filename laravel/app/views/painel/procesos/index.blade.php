@section('content')

    <legend>
        <h2>Procesos</h2>
    </legend>

    {{ Form::model($procesos, [
        'route' => ['painel.procesos.update', $procesos->id],
        'method' => 'patch'])
    }}

        @include('painel.procesos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop