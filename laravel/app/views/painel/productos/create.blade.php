@section('content')

    <legend>
        <h2>Adicionar Produto</h2>
    </legend>

    {{ Form::open(['route' => 'painel.productos.store', 'files' => true]) }}

        @include('painel.productos._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop