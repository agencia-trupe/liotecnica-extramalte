@section('content')

    <legend>
        <h2>Editar Produto</h2>
    </legend>

    {{ Form::model($producto, [
        'route' => ['painel.productos.update', $producto->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.productos._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop