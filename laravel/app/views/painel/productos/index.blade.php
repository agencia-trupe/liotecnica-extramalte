@section('content')

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>
            Produtos
            <div class="btn-group pull-right">
                <a href="{{ route('painel.productos.entrada.index') }}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span> Editar página de entrada</a>
                <a href="{{ route('painel.productos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Produto</a>
            </div>
        </h2>
    </legend>

    @if(count($productos))
    <table class="table table-striped table-bordered table-hover table-sortable" data-tabela="productos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($productos as $producto)

            <tr class="tr-row" id="id_{{ $producto->id }}">
                <td style="width:74px;text-align:center;">
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $producto->titulo }}</td>
                <td><img src="{{ url('assets/img/productos/'.$producto->imagem) }}" style="max-width:180px;height:auto;" class="img-responsive"></td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.productos.destroy', $producto->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.productos.edit', $producto->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhum produto cadastrado.</div>
    @endif

@stop