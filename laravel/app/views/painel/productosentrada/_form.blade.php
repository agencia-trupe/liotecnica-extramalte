@if($errors->any())
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    @foreach($errors->all() as $error)
            <p>{{ $error }}</p>
    @endforeach
    </div>
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('texto', 'Texto') }}
        {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'productos']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('texto_en', 'Texto [INGLÊS]') }}
        {{ Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'productos']) }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('polvo_desc', 'Descrição: Polvo (5 linhas)') }}
        {{ Form::textarea('polvo_desc', null, ['class' => 'form-control ckeditor', 'data-editor' => 'productos']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('polvo_desc_en', 'Descrição: Polvo (5 linhas) [INGLÊS]') }}
        {{ Form::textarea('polvo_desc_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'productos']) }}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {{ Form::label('liquido_desc', 'Descrição: Líquido (5 linhas)') }}
        {{ Form::textarea('liquido_desc', null, ['class' => 'form-control ckeditor', 'data-editor' => 'productos']) }}
    </div>
    <div class="form-group col-md-6">
        {{ Form::label('liquido_desc_en', 'Descrição: Líquido (5 linhas) [INGLÊS]') }}
        {{ Form::textarea('liquido_desc_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'productos']) }}
    </div>
</div>

<div class="form-group well">
    <label>Composición Nutricional Típica</label>
    <table class="table table-striped table-bordered table-hover" style="margin:10px 0 0;">
        <tr>
            <th style="vertical-align:middle;text-align:right;padding-right:20px;height:60px;vertical-align:middle;"><span class="glyphicon glyphicon-menu-down"></span></th>
            <th style="vertical-align:middle;">Extracto de Malta en polvo</th>
            <th style="vertical-align:middle;">Extracto de Malta líquido</th>
        </tr>
        <tr>
            <td style="vertical-align:middle;text-align:right;padding-right:20px;">Calorias (kcal)</td>
            <td>{{ Form::text('polvo_calorias', null, ['class' => 'form-control']) }}</td>
            <td>{{ Form::text('liquido_calorias', null, ['class' => 'form-control']) }}</td>
        </tr>
        <tr>
            <td style="vertical-align:middle;text-align:right;padding-right:20px;">Carbohidratos (g)</td>
            <td>{{ Form::text('polvo_carbohidratos', null, ['class' => 'form-control']) }}</td>
            <td>{{ Form::text('liquido_carbohidratos', null, ['class' => 'form-control']) }}</td>
        </tr>
        <tr>
            <td style="vertical-align:middle;text-align:right;padding-right:20px;">Fibras (g)</td>
            <td>{{ Form::text('polvo_fibras', null, ['class' => 'form-control']) }}</td>
            <td>{{ Form::text('liquido_fibras', null, ['class' => 'form-control']) }}</td>
        </tr>
        <tr>
            <td style="vertical-align:middle;text-align:right;padding-right:20px;">Proteinas (g)</td>
            <td>{{ Form::text('polvo_proteina', null, ['class' => 'form-control']) }}</td>
            <td>{{ Form::text('liquido_proteina', null, ['class' => 'form-control']) }}</td>
        </tr>
        <tr>
            <td style="vertical-align:middle;text-align:right;padding-right:20px;">Grasas Totales (g)</td>
            <td>{{ Form::text('polvo_grasas_totales', null, ['class' => 'form-control']) }}</td>
            <td>{{ Form::text('liquido_grasas_totales', null, ['class' => 'form-control']) }}</td>
        </tr>
        <tr>
            <td style="vertical-align:middle;text-align:right;padding-right:20px;">Grasas Trans (g)</td>
            <td>{{ Form::text('polvo_grasas_trans', null, ['class' => 'form-control']) }}</td>
            <td>{{ Form::text('liquido_grasas_trans', null, ['class' => 'form-control']) }}</td>
        </tr>
    </table>
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}