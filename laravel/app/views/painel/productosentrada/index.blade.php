@section('content')

    <legend>
        <h2><small>Produtos /</small> Página de Entrada</h2>
    </legend>

    {{ Form::model($entrada, [
        'route' => ['painel.productos.entrada.update', $entrada->id],
        'method' => 'patch'])
    }}

        @include('painel.productosentrada._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop