-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 18-Mar-2015 às 15:35
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `liotecnica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aplicaciones`
--

CREATE TABLE IF NOT EXISTS `aplicaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `aplicaciones`
--

INSERT INTO `aplicaciones` (`id`, `imagem`, `nome`, `descricao`, `ordem`, `created_at`, `updated_at`) VALUES
(1, '20150317122439_bebidas-lacteas.jpg', 'Bebidas lácteas / Bebidas UHT / Bebidas achocolatadas', '<ul>\r\n	<li>Redondea y complementa el sabor del chocolate.</li>\r\n	<li>Oculta notas indeseables provenientes del cacao.</li>\r\n	<li>Endulza de forma suave y natural.</li>\r\n	<li>Proporciona um sabor malteado.</li>\r\n	<li>Proporciona buen cuerpo y sensaci&oacute;n de cremosidad.</li>\r\n	<li>Fortalece los conceptos de nutrici&oacute;n, naturalidad y salud</li>\r\n</ul>\r\n\r\n<p>Dosificaci&oacute;n indicativa: Empezar con 2,0%</p>\r\n\r\n<p>Dosificaci&oacute;n indicativa para produtos con sabor malteado: empezar com 10,0%</p>\r\n', -1, '2015-03-17 15:24:41', '2015-03-17 17:02:56'),
(2, '20150317122454_barra-de-cereales.jpg', 'Barra de Cereales', '<p>...</p>\r\n', -1, '2015-03-17 15:24:54', '2015-03-17 15:24:54'),
(3, '20150317122508_cereales-para-desayuno.jpg', 'Cereales para el desayuno y Granolas', '<p>...</p>\r\n', -1, '2015-03-17 15:25:08', '2015-03-17 15:25:08'),
(4, '20150317122524_productos-panaderia.jpg', 'Productos de Panadería', '<p>...</p>\r\n', -1, '2015-03-17 15:25:24', '2015-03-17 15:25:24'),
(5, '20150317122533_crackers.jpg', 'Crackers', '<p>...</p>\r\n', -1, '2015-03-17 15:25:33', '2015-03-17 15:25:33'),
(6, '20150317122543_galetas-dulces.jpg', 'Galletas dulces', '<p>...</p>\r\n', -1, '2015-03-17 15:25:43', '2015-03-17 15:25:43'),
(7, '20150317122552_chocolate.jpg', 'Chocolate', '<p>...</p>\r\n', -1, '2015-03-17 15:25:52', '2015-03-17 15:25:52'),
(8, '20150317122600_helados.jpg', 'Helados', '<p>...</p>\r\n', -1, '2015-03-17 15:26:00', '2015-03-17 15:26:00'),
(9, '20150317122612_chocolate-polvo.jpg', 'Chocolate en polvo', '<p>...</p>\r\n', -1, '2015-03-17 15:26:12', '2015-03-17 15:26:12'),
(10, '20150317122619_postres.jpg', 'Postres', '<p>...</p>\r\n', -1, '2015-03-17 15:26:20', '2015-03-17 15:26:20'),
(11, '20150317122628_cervezas.jpg', 'Cervezas', '<p>...</p>\r\n', -1, '2015-03-17 15:26:28', '2015-03-17 15:26:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `beneficios`
--

CREATE TABLE IF NOT EXISTS `beneficios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sensoriales` text COLLATE utf8_unicode_ci NOT NULL,
  `tecnologicos` text COLLATE utf8_unicode_ci NOT NULL,
  `nutricionales` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `beneficios`
--

INSERT INTO `beneficios` (`id`, `sensoriales`, `tecnologicos`, `nutricionales`, `created_at`, `updated_at`) VALUES
(1, '<ul>\r\n	<li>Sabor t&iacute;pico de malta, aroma suave y agradable</li>\r\n	<li>Edulcoraci&oacute;n suave de origen natural, fabricado a partir de &nbsp;granos integrales&nbsp;</li>\r\n	<li>Resalta y complementa el sabor de los alimentos</li>\r\n	<li>Aporta crocancia y favorece el pardeamiento</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Sustrato para la fermentaci&oacute;n en los productos de panader&iacute;a</li>\r\n	<li>Favorece la reacci&oacute;n de Maillard, desarrollando una coloraci&oacute;n dorada y uniforme,&nbsp;</li>\r\n	<li>Redondea sabores y oculta notas indeseables en productos con cacao.</li>\r\n	<li>Retiene la humedad de los productos de panader&iacute;a, aumentando vida &uacute;til.</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Fuente de az&uacute;cares saludables, de metabolizaci&oacute;n diferenciada y f&aacute;cil absorci&oacute;n.</li>\r\n	<li>Importante fuente de energ&iacute;a</li>\r\n	<li>Contiene amino&aacute;cidos, vitaminas y minerales naturales provenientes de granos integrales&nbsp;</li>\r\n	<li>Bajo nivel de grasas y libre de grasas trans</li>\r\n	<li>Componentes antioxidantes</li>\r\n</ul>\r\n', '0000-00-00 00:00:00', '2015-03-17 17:51:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `calidad`
--

CREATE TABLE IF NOT EXISTS `calidad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto2` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `calidad`
--

INSERT INTO `calidad` (`id`, `texto1`, `texto2`, `imagem`, `selos`, `created_at`, `updated_at`) VALUES
(1, '<p>Basada en las principales normas internacionales de gesti&oacute;n de calidad, LIOT&Eacute;CNICA ha desarrollado un sistema &uacute;nico e integrado para el incremento continuo de personas, productos, procesos y del ambiente, que se llama <strong>SGL - SISTEMA DE GESTI&Oacute;N LIOT&Eacute;CNICA</strong>. El sistema incluye:&nbsp;</p>\r\n\r\n<ul>\r\n	<li>Certificaci&oacute;n FSSC 22000</li>\r\n</ul>\r\n\r\n<p>&nbsp;Atiende a las exigencias de:</p>\r\n\r\n<ul>\r\n	<li>OHSAS 18001</li>\r\n	<li>ISO 14000</li>\r\n	<li>ISO 17025</li>\r\n	<li>SA 8000</li>\r\n	<li>Integridad de los negocios</li>\r\n	<li>Programa 5S</li>\r\n</ul>\r\n', '<p>La f&aacute;brica de Extracto de Malta de Liot&eacute;cnica posee la certificaci&oacute;n FSSC 22000 , garantizando &nbsp;la seguridad de sua productos por medio de un equipo t&eacute;cnico calificado y tambi&eacute;n mediante programas de entrenamiento de personas, con foco en la mejora continua de productos y procesos.</p>\r\n\r\n<p>El equipo de calidad act&uacute;a en los controles de procesos de producci&oacute;n, desde el recibimiento de las materias primas hasta la expedici&oacute;n del producto final, atendiendo a especificaciones t&eacute;cnicas, cumpliendo con todos los requisitos legales y rastreabilidad. Un programa de muestreo de las producciones garantiza la conformidad de los ingredientes producidos en nuestras f&aacute;bricas.</p>\r\n\r\n<p>Nuestros proveedores son calificados y los procesos de producci&oacute;n son frecuentemente sometidos a auditor&iacute;as de Buenas Pr&aacute;cticas de Manufactura (BPM) y Est&aacute;ndarizaciones de procesos.</p>\r\n\r\n<p>Contamos con laboratorios internos de an&aacute;lisis aensorial, an&aacute;lisis microbiol&oacute;gicos y f&iacute;sico-qu&iacute;micos que son avalados todos los a&ntilde;os por el LGC -Standards Proficiency Tests (Reino Unido).</p>\r\n', '20150317155800_calidad-img.png', '20150317155800_calidad-selos.png', '0000-00-00 00:00:00', '2015-03-17 19:59:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `endereco` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contacto`
--

INSERT INTO `contacto` (`id`, `endereco`, `telefone`, `email`, `google_maps`, `created_at`, `updated_at`) VALUES
(1, 'Av. João Paulo I, 1098 - Embu - SP - Brasil - 06817-000', '+55 11 4785-2386 / 4785-2370', 'export@liotecnica.com.br', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.0025811876153!2d-46.81659159999998!3d-23.6400786!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce54afde03bb49%3A0x3a4d37210f0eb7cf!2sAv.+Jo%C3%A3o+Paulo+I%2C+1098+-+Jardim+Sao+Luiz%2C+Embu+das+Artes+-+SP%2C+06816-550!5e0!3m2!1spt-BR!2sbr!4v1426612792529" width="100%" height="100%" frameborder="0" style="border:0"></iframe>', '0000-00-00 00:00:00', '2015-03-17 20:20:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contactos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contactos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prefijo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `primer_nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apelido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `codigo_postal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensaje` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'false',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `historia`
--

CREATE TABLE IF NOT EXISTS `historia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `historia`
--

INSERT INTO `historia` (`id`, `texto`, `imagem`, `created_at`, `updated_at`) VALUES
(3, '<p>Liot&eacute;cnica es una compa&ntilde;&iacute;a especializada en proveer las mejores soluciones en ingredientes. Acompa&ntilde;a las tendencias mundiales del mercado de alimentos, buscando siempre productos saludables, pr&aacute;cticos y convenientes. Actualmente, tiene 3 unidades de producci&oacute;n, ubicadas en el Estado de S&atilde;o Paulo, un importante punto log&iacute;stico y estrat&eacute;gico en Brasil.</p>\r\n\r\n<p>Desde el a&ntilde;o 1964, su compromiso con la calidad, las constantes inversiones en nuevas tecnolog&iacute;as y la voluntad de proporcionar lo mejor hace que Liot&eacute;cnica sea una compa&ntilde;&iacute;a s&oacute;lida y reconocida en el segmento de alimentos.</p>\r\n\r\n<p>Presente con su l&iacute;nea de ingredientes liofilizados en grandes players del mercado multinacional, la compa&ntilde;&iacute;a empieza a ofrecer una l&iacute;nea diversificada de Extractos de Malta, &nbsp;l&iacute;quidos y en polvo. El cual es un ingrediente vers&aacute;til, con beneficios sensoriales, tecnol&oacute;gicos y de gran importancia nutricional.</p>\r\n\r\n<h3>Liot&eacute;cnica: naturalmente, &iexcl;su mejor opci&oacute;n en ingredientes!</h3>\r\n', '20150316174931_img-nuestraHistoria-liotecnica.png', '0000-00-00 00:00:00', '2015-03-16 20:49:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `homepage`
--

CREATE TABLE IF NOT EXISTS `homepage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `homepage`
--

INSERT INTO `homepage` (`id`, `titulo`, `chamada1`, `chamada2`, `frase`, `created_at`, `updated_at`) VALUES
(3, 'Extracto de Malta', 'Naturalmente, sua melhor opção em ingredientes.', 'Mais sabor e nutrição para seus produtos', 'Um ingrediente versátil com benefícios sensoriais, tecnológicos e de grande importância nutricional.', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_28_130645_create_usuarios_table', 1),
('2015_03_13_135113_create_homepage_table', 1),
('2015_03_13_135128_create_historia_table', 1),
('2015_03_13_135144_create_productos_entrada_table', 1),
('2015_03_13_135155_create_productos_table', 2),
('2015_03_13_135210_create_aplicaciones_table', 2),
('2015_03_13_135225_create_beneficios_table', 2),
('2015_03_13_135245_create_procesos_table', 2),
('2015_03_13_135256_create_calidad_table', 2),
('2015_03_13_135312_create_contactos_recebidos_table', 2),
('2015_03_13_135322_create_contacto_table', 2),
('2015_03_13_141407_create_producto_aplicacion_table', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `procesos`
--

CREATE TABLE IF NOT EXISTS `procesos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `procesos`
--

INSERT INTO `procesos` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Los granos son molidos y mezclados con agua, y bajo condiciones est&aacute;ndares de temperatura y tiempo, ocurre la hidr&oacute;lisis del almid&oacute;n y de las prote&iacute;nas, que se solubilizan en su fase l&iacute;quida, con las vitaminas y minerales que existen naturalmente en los granos.</p>\r\n\r\n<p>Despu&eacute;s, ocurre la filtraci&oacute;n, y ese alm&iacute;bar - que contiene los nutrientes solubles - es concentrado por medio de evaporaci&oacute;n al vac&iacute;o, resultando en un jarabe con 80% de s&oacute;lidos.</p>\r\n\r\n<p>Este jarabe es envasado y denominado extracto de malta l&iacute;quido.</p>\r\n\r\n<p>Para fabricar el extracto de malta en polvo, se realiza la deshidrataci&oacute;n al vac&iacute;o del jarabe y posteriormente el proceso de molienda y empaque.</p>\r\n', '0000-00-00 00:00:00', '2015-03-17 21:59:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ingredientes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `envase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `peso` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vida_util` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `humedad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kosher` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `actividad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `productos`
--

INSERT INTO `productos` (`id`, `tipo`, `titulo`, `imagem`, `ingredientes`, `envase`, `peso`, `vida_util`, `humedad`, `kosher`, `actividad`, `ordem`, `created_at`, `updated_at`) VALUES
(2, 'polvo', 'Extracto de Malta Dry Standard', '20150316184108_img-dry-standard.jpg', 'Cebada cruda y malteada', 'Kraft', '25kg', '24 meses', 'Máx. 4,0', 'Disponible', 'Não Diastásico', 0, '2015-03-16 21:41:08', '2015-03-17 15:27:28'),
(3, 'polvo', 'Extracto de Malta Dry Sweet', '20150316184140_img-dry-sweet.jpg', '1', '1', '1', '1', '1', '1', '1', 1, '2015-03-16 21:41:40', '2015-03-16 21:41:40'),
(4, 'polvo', 'Extracto de Malta Dry Toast', '20150316184151_img-dry-toast.jpg', '1', '1', '1', '1', '1', '1', '1', 2, '2015-03-16 21:41:51', '2015-03-16 21:41:51'),
(5, 'polvo', 'Extracto de Malta Dry Intense', '20150316184252_img-dry-intense.jpg', '1', '1', '1', '1', '1', '1', '1', 3, '2015-03-16 21:42:52', '2015-03-16 21:42:52'),
(6, 'polvo', 'Extracto de Malta Dry Medium', '20150316184303_img-dry-medium.jpg', '1', '1', '1', '1', '1', '1', '1', 4, '2015-03-16 21:43:03', '2015-03-16 21:43:03'),
(7, 'polvo', 'Extracto de Malta Dry Brew', '20150316184312_img-dry-brew.jpg', '1', '1', '1', '1', '1', '1', '1', 5, '2015-03-16 21:43:12', '2015-03-16 21:43:12'),
(8, 'liquido', 'Extracto de Malta Liquid Golden Light', '20150316184333_img-liquid-light.jpg', '1', '1', '1', '1', '1', '1', '1', 6, '2015-03-16 21:43:34', '2015-03-16 21:43:34'),
(9, 'liquido', 'Extracto de Malta Liquid Golden Medium', '20150316184350_img-liquid-medium.jpg', '1', '1', '1', '1', '1', '1', '1', 7, '2015-03-16 21:43:50', '2015-03-16 21:43:50'),
(10, 'liquido', 'Extracto de Malta Liquid Golden Dark', '20150316184401_img-liquid-dark.jpg', '1', '1', '1', '1', '1', '1', '1', 8, '2015-03-16 21:44:01', '2015-03-16 21:44:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `productos_entrada`
--

CREATE TABLE IF NOT EXISTS `productos_entrada` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `polvo_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `polvo_calorias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_carbohidratos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_fibras` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_proteina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_grasas_totales` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `polvo_grasas_trans` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `liquido_calorias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_carbohidratos` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_fibras` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_proteina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_grasas_totales` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liquido_grasas_trans` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `productos_entrada`
--

INSERT INTO `productos_entrada` (`id`, `texto`, `polvo_desc`, `polvo_calorias`, `polvo_carbohidratos`, `polvo_fibras`, `polvo_proteina`, `polvo_grasas_totales`, `polvo_grasas_trans`, `liquido_desc`, `liquido_calorias`, `liquido_carbohidratos`, `liquido_fibras`, `liquido_proteina`, `liquido_grasas_totales`, `liquido_grasas_trans`, `created_at`, `updated_at`) VALUES
(1, '<p>El Extracto de Malta es un ingrediente que resulta del proceso de maceraci&oacute;n de los granos integrales de cebada malteada y cebada, con el objetivo de promover la &nbsp;hidr&oacute;lisis del almid&oacute;n y de las prote&iacute;nas; para obtener un edulcorante natural, con un perfil nutricional superior al del az&uacute;car convencional.</p>\r\n\r\n<p>La maltosa es el componenete principal, aunque tambi&eacute;n contiene prote&iacute;nas hidrolizadas, vitaminas y minerales. Nuestro proceso es completamente automatizado y controlado, y no requiere la inclusi&oacute;n de aditivos, permitiendo de esta forma que los productos en los cuales el extracto de malta es aplicado, puedan contar el claim de naturalidad.</p>\r\n', '<p>Polvo fino&nbsp;<br />\r\nColor crema&nbsp;<br />\r\nSabor y aroma t&iacute;picos de malta<br />\r\n96% de s&oacute;lidos solubles&nbsp;</p>\r\n', '380 - 400 kcal/100g', '88 - 93%', '0%', '2,0 - 7,0%', '0,4 - 0,8%', '0%', '<p>L&iacute;quido denso y viscoso&nbsp;<br />\r\nColor dorado<br />\r\nSabor y aroma t&iacute;picos de malta<br />\r\n78 - 82% de s&oacute;lidos solubles</p>\r\n', '300 - 330 kcal/100g', '70 - 78%', '0%', '1,6 - 5,2%', '0,2 - 0,5%', '0%', '0000-00-00 00:00:00', '2015-03-16 21:07:01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `producto_aplicacion`
--

CREATE TABLE IF NOT EXISTS `producto_aplicacion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `aplicacion_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Extraindo dados da tabela `producto_aplicacion`
--

INSERT INTO `producto_aplicacion` (`id`, `producto_id`, `aplicacion_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 2, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 3, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 3, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 4, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 4, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 4, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 4, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 4, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 4, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 4, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 4, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 4, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 4, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 5, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 6, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 6, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 7, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 7, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 8, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 8, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 8, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 8, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 9, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 9, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 9, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 9, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 10, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_email_unique` (`email`),
  UNIQUE KEY `usuarios_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'contato@trupe.net', 'trupe', '$2y$10$bnyZztgEvP9Rn9i4dfLEVeKg0OK/f3OUW.siVeuBIN/d/S6CiaDOe', 'mGoXbqu4aaDkmlCuRvwLi1LM0j2TZgSKhOKZidyHAiQ28guyp6wg6B3XD4Ln', '0000-00-00 00:00:00', '2015-03-17 15:24:10');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
